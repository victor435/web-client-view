/*
 * @Description:
 * @Author: 李大玄
 * @Date: 2022-07-16 15:25:03
 * @FilePath: /data-config-view/babel.config.js
 * @LastEditors: 李大玄
 * @LastEditTime: 2022-07-18 19:00:28
 */
module.exports = {
  presets: [
    ["@vue/app"]
  ],
  compact: true,
  plugins: [
    // [
    //   "component",
    //   {
    //     libraryName: "element-ui",
    //     styleLibraryName: "~node_modules/el-ui/theme"
    //   }
    // ]
  ]
};