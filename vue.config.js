/*
 * @Description:
 * @Author: 李大玄
 * @Date: 2022-07-16 16:11:20
 * @FilePath: /hw-web-client-view/vue.config.js
 * @LastEditors: 段丽军
 * @LastEditTime: 2023-12-26 14:27:37
 */

// 代码压缩js
// 多进程打包
const os = require("os");
const OptimizeCSSPlugin = require("optimize-css-assets-webpack-plugin");
// 进度条
const WebpackBar = require("webpackbar");
const ProgressBarWebpackPlugin = require("progress-bar-webpack-plugin");
const path = require("path");
const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");

// 是否为生产环境
const cdn = {
  css: [
    "//statics.easyliao.com/web/public/plugin/nprogress/0.2.0/nprogress.min.css"
  ],
  js: [
    "http://statics.easyliao.com/web/project/env_loginUrl.js",
    "//statics.easyliao.com/web/vue/vue/2.6.14/vue.min.js",
    "//statics.easyliao.com/web/public/plugin/nprogress/0.2.0/nprogress.min.js",
    "//statics.easyliao.com/web/vue/vuex/3.6.2/vuex.min.js",
    "//statics.easyliao.com/web/vue/axios/0.21.1/axios.min.js",
    "//statics.easyliao.com/web/public/plugin/lodash/4.17.21/lodash.min.js",
    "//statics.easyliao.com/web/vue/vue-router/3.5.2/vue-router.min.js",
    "//statics.easyliao.com/web/el-web-basic/vuedraggable/Sortable.min.js",
    "//statics.easyliao.com/web/el-web-basic/vuedraggable/dist/vuedraggable.umd.js",
    "//statics.easyliao.com/web/el-web-basic/wangeditor/wangEditor.min.js"
  ]
};
const argv = require("plat/src/utils/getargv");

const version = process.env[`VUE_APP_VERSION_${argv.project}`];
console.log("argv", argv.project, version);
module.exports = {
  productionSourceMap: false,
  lintOnSave: false, //去掉eslint检验
  publicPath: "",
  outputDir: `dist/${version}`,
  // 多线程打包
  parallel: os.cpus().length / 2,
  css: {
    extract: true,
    requireModuleExtension: true,
    sourceMap: false
    // loaderOptions: {
    // 	sass: {
    // 		prependData: `@import "~ui@/assets/base/common/index.scss";`
    // 	}
    // }
  },
  chainWebpack: (config) => {
    config.plugin("provide").use(webpack.ProvidePlugin, [{
      "window.Quill": "quill"
    }]);
    // 清除css，js版本号
    config.output.filename("js/[name].js").end();
    config.output.chunkFilename("js/[name].js").end();
    config.plugin("extract-css").tap(() => [
      {filename: "css/[name].css", chunkFilename: "css/[name].css"}
    ]);

    // 解决ie11兼容ES6
    // config.entry("main").add("babel-polyfill");

    // 生产环境或本地需要cdn时，才注入cdn  lib
    if (argv.islib) {
      config.plugin("html").use(HtmlWebpackPlugin).tap((args) => {
        args.cdn = cdn;
        return args;
      });
    } else {
      config.plugin("html").tap((args) => {
        args[0].cdn = cdn;
        return args;
      });
    }

    // const oneOfsMap = config.module.rule("scss").oneOfs.store;
    // oneOfsMap.forEach((item) => {
    //     item
    //         .use("sass-resources-loader")
    //         .loader("sass-resources-loader")
    //         .options({
    //             // 引入多个全局sass文件
    //             resources: ["./src/assets/base/common/index.scss"]
    //         })
    //         .end();
    // });
    config.resolve.alias
        .set("plat@", path.join(__dirname, "node_modules/plat"))
        .set("ui@", path.join(__dirname, "node_modules/el-ui"))
        .set("@", path.join(__dirname, "src"))
        .set("@assets", path.join(__dirname, "src/assets"))
        .set("@scss", path.join(__dirname, "src/assets/style"));


    config.optimization.chunkIds = "deterministic";
    config.plugins.delete("prefetch");
    config.plugins.delete("preload");
  },
  /*
  name: 参数配置 例如项目名称叫 abc-web-view
  asdasd 需要改成 abc-view
  */
  configureWebpack: (config) => {
    config.output.library = "abc-view"; //
    config.output.libraryTarget = "umd"; // umd
    config.output.umdNamedDefine = true;
    config.output.libraryExport = "abc-view";
    config.output.jsonpFunction = "webpackJsonp_abc-view";
    config.entry.app = path.join(__dirname, "src", "application", "index.js");
    config.plugins.push(new WebpackBar(), new ProgressBarWebpackPlugin());

    // 生产环境相关配置
    config.externals = {
      vue: "Vue",
      vuex: "Vuex",
      axios: "axios",
      lodash: "_",
      "vuedraggable": "vuedraggable",
      "sortable": "Sortable",
      "window.wangEditor": "wangEditor",
      "vue-router": "VueRouter",
      echarts: "echarts"
      // 'element-ui': 'ELEMENT',
      // "nprogress": "nprogress",
    };
    //关闭 webpack 的性能提示
    config.performance = {
      hints: false
    };
    // 去掉注释
    config.plugins.push(
        // new SentryCliPlugin({
        //     include: "./dist", // 打包后的文件夹
        //     release: process.env.RELEASE_VERSION, // 引用配置的版本号，版本号需要一致
        //     configFile: "sentry.properties",
        //     ignoreFile: ".gitignore", // 指定忽略文件配置
        //     ignore: ["node_modules", "vue.config.js"],
        //     // configFile: './.sentryclirc',   // 指定sentry上传配置
        //     urlPrefix: "~/" // 保持与publicpath相符
        // }),
        //清除dist目录的插件
        // new CleanWebpackPlugin(),
        // new OptimizeCSSPlugin({
        // 	cssProcessorOptions: {
        // 		safe: true
        // 	},
        // 	sourceMap: false,
        // 	cssnanoOptions: {preset: ["default", {mergeLonghand: false, cssDeclarationSorter: false}]}
        // })
    );
  },
  devServer: {
    port: 4101,
    open: true,
    headers: {
      "Access-Control-Allow-Origin": "*"
    },
    proxy: {
      "/entest": {
        target: "https://test19.easyliao.net",
        changeOrigin: true,
        loglevel: "debug",
        pathRewrite: {
          "^/entest": ""
        }
      },
      "/enprebase": {
        target: "https://pre.upbeat.chat",
        changeOrigin: true,
        loglevel: "debug",
        pathRewrite: {
          "^/enprebase": ""
        }
      },
      "/enpreauth": {
        target: "https://pre-auth.upbeat.chat",
        changeOrigin: true,
        loglevel: "debug",
        pathRewrite: {
          "^/enpreauth": ""
        }
      },
      "/cnprebase": {
        target: "https://prd19.easyliao.com",
        changeOrigin: true,
        loglevel: "debug",
        pathRewrite: {
          "^/cnprebase": ""
        }
      },
      "/cnpreauth": {
        target: "https://pre-auth.easyliao.com",
        changeOrigin: true,
        loglevel: "debug",
        pathRewrite: {
          "^/cnpreauth": ""
        }
      },
      "/cntest": {
        target: "https://test-prd18.easyliao.net",
        changeOrigin: true,
        loglevel: "debug",
        pathRewrite: {
          "^/cntest": ""
        }
      },


      "/hw": {
        target: "https://im-api.upbeat.chat",
        changeOrigin: true,
        loglevel: "debug",
        pathRewrite: {
          "^/hw": ""
        }
      },
      "/testHw": {
        target: "https://test19.easyliao.net",
        changeOrigin: true,
        loglevel: "debug",
        pathRewrite: {
          "^/testHw": ""
        }
      }
    }
  }
};