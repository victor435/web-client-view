/*
 * @Description:
 * @Author: 李大玄
 * @Date: 2022-07-16 17:12:33
 * @FilePath: /data-config-view/public/load.js
 * @LastEditors: 李大玄
 * @LastEditTime: 2022-08-02 11:49:42
 */

const fileName = [
  "//statics.easyliao.com/web/vue/vue/2.6.14/vue.js",
  "//statics.easyliao.com/web/vue/vuex/3.6.2/vuex.min.js",
  "//statics.easyliao.com/web/public/plugin/lodash/4.17.21/lodash.min.js",
  "//statics.easyliao.com/web/public/plugin/nprogress/0.2.0/nprogress.min.js",
  "//statics.easyliao.com/web/public/plugin/nprogress/0.2.0/nprogress.min.css",
  "//statics.easyliao.com/web/vue/axios/0.21.1/axios.min.js",
  // // '//statics.easyliao.com/web/vue/element-ui/2.15.3/locale/zh-CN.js',
  // '//statics.easyliao.com/web/vue/element-ui/2.15.3/index.js',
  "//statics.easyliao.com/web/vue/vue-router/3.5.2/vue-router.min.js",

  "//statics.easyliao.com/web/el-web-basic/vuedraggable/Sortable.min.js",
  "//statics.easyliao.com/web/el-web-basic/vuedraggable/dist/vuedraggable.umd.js",
  "//statics.easyliao.com/web/el-web-basic/wangeditor/wangEditor.min.js"
];

addLinkArr(fileName);
function addLinkArr(srcArr) {
  let arr = srcArr;
  arr.forEach((item) => {
    if (/\.(?:css)$/.test(item)) {
      this.addCssByLink(item);
    } else if (item.match(".css")) {
      this.addCssByLink(item);
    } else if (/\.(?:js)$/.test(item)) {
      this.addJsByScript(item);
    }
  });
}

function addCssByLink(url) {
  var doc = document;
  var link = doc.createElement("link");
  link.setAttribute("rel", "stylesheet");
  link.setAttribute("type", "text/css");
  link.setAttribute("href", url);
  var heads = doc.getElementsByTagName("head");
  if (heads.length) {
    heads[0].appendChild(link);
  } else {
    doc.documentElement.appendChild(link);
  }
}

function addJsByScript(url) {
  var doc = document;
  const s = document.createElement("script");
  // s.type = "module";
  s.src = url;
  var heads = doc.getElementsByTagName("head");
  if (heads.length) {
    heads[0].appendChild(s);
  }
}

console.log("20240516 v1.1.5 15:00 - hw")




