/*
 * @Description:
 * @Author: 李大玄
 * @Date: 2023-11-27 09:37:41
 * @FilePath: /hw-web-client-view/src/utils/config/lang/cnConfig.js
 * @LastEditors: 段丽军
 * @LastEditTime: 2023-12-27 11:36:55
 */
const cnConfig = {
  type: "cn",
  className: ["default"],
  layoutTop: {
    monitorDialogue: "监控对话",
    logoutText: "退出登录"
  },
  welcomeMessage: "欢迎登录群峰后台管理系统",
  cancelText:  "取消",
  ConfirmText:  "确认",
  TipText: "提示",
  requestFailed:  "请求失败",
  requestResponseTips: "响应错误",
  operSuccess: "操作成功",
  operationCompleted: "操作成功",
  chat: {
    chatList: {
      current: "当前对话",
      end: "已结束对话",
      inputSearchPlaceholder: "请输入内容",
      myChat: "我的对话",
      robotChat: "机器人对话",
      takeoverError: "抢接失败",
      noConversationsAvailable: "暂无会话",
      noNicknameFound: "未获取到昵称",
      files: "文件",
      images: "图片",
      emojis: "表情",
      form: "表单",
      visitorInformation: "表单已提交",
      noNewsAvailable: "暂无最新消息",
      secondText: "秒",
      minuteText: "分",
      hourText: "时",
      history: "消息记录",
      waitText: "用户已等待",
      endTabBottomText: "已拉取30分钟内或不超上限50条对话，查看更多请打开"
    },
    accountOffline:  {
      accountOfflineText: "账号下线",
      btnText: "重新登录",
      contentText: "账号已掉线，当前账号在易聊客户端登录，请重新登录"

    },
    chatContent: {
      msgTimeLabel: "昨天",
      noDataTips: "选择左侧用户列表开始会话",
      seizeChatBtn: "抢接",
      transferBtn: "转接",
      endConversationBtn: "结束会话",
      visitor: "访客",
      ai: "机器人",
      agent: "客服",
      tip: "提示",
      transferTips: "对话转接后将需要您人工接待，且无法交还给机器人",
      expandBtn: "展开",
      collapseBtn: "收起",
      leadNotificationResults: "名片推送结果：",
      leadNotificationSuccess: "【名片推送】xxx-成功 ",
      leadNotificationFailed: "【名片推送】xxx-失败",
      viewDetails: "查看详情",
      inputSendPlaceholder: "请输入您要咨询的内容…",
      emoji: "表情",
      image: "图片",
      sendAreaEndTips: "对话结束无法发送信息",
      sendAreaMonitoringTips: "监听对话无法发送消息",
      sendImageFormatTips: "仅支持.jpg，.jpeg，.png格式图片发送",
      sendImageSizeTips: "上传图片不能超过1M",
      requestResponseTips: "响应错误",
      imageUploadError: "图片上传错误",
      emptyMsgTips: "消息为空",
      sendLongTips: "发送内容不能操作1000个字符，当前内容长度为：",
      sendBtn: "发送",
      notSupportMsgTips: "暂不支持此消息类型，请登录客户端查看",
      loadingTips: "加载中",
      loadFailTips: "加载失败",
      visitorLeadTitle: "访客名片",
      generalResponsesTitle: "常用语",
      LeadFormRequiredTips: "必填",
      LeadSaveLeadBtn: "保存名片",
      LeadOptionUnknown: "未知",
      LeadOptionMan: "男",
      LeadOptionWoman: "女",
      LeadOperSuccess: "操作成功",
      LeadFormNotSupportFieldTips: "暂不支持类型",
      GeneralInputSearchPlaceholder: "请输入搜索内容",
      GeneralSendBtn: "发送",
      GeneralLoadingTips: "加载中",
      GeneralNoMoreTips: "没有更多了",
      loadMore: "加载更多",
      loadEndPre: "仅展示最近100条消息，进“",
      loadEndSuf: "”查看更多",
    }
  },
  messageNotification: {
    text: "消息提醒设置",
    soundNotification: "声音提醒",
    bubbleNotification: "气泡提醒",
    youConversation: "您新到一个对话",
    youMessage: "您新到一条消息",
    colleagueConversation: "同事新到一个对话",
    colleagueMessage: "同事新到一条消息"
  },
  monitorDialogue: {
    title: "客服监听",
    inputSearchPlaceholder: "搜索",
    deptDefault: "公司名称",
    notData: "暂无数据",
    selectAll: "全部",
    selected: "已选中",
    notSelectPlaceholder: "您尚未选择任何人",
    maxCountTips: "超过最大值：",
    btnCancel: "取消",
    btnSave: "保存"
  },
  serviceCode: {
    "90000": "未检测到token或token已过期",
    "90001": "未检测到token, 无权访问",
    "90002": "token无效",
    "90003": "账号无权访问此应用",
    "90004": "token认证中心校验失败",
    "90005": "账号无权访问此资源",
    "90006": "需要身份确认",
    "90010": "无法获取认证账号",
    "90011": "无法解析ProductId",
    "401": "token无效",
    "403": "token无效"
  }
};

export default cnConfig;