/*
 * @Description:
 * @Author: 李大玄
 * @Date: 2023-11-27 09:37:56
 * @FilePath: /hw-web-client-view/src/utils/config/lang/enConfig.js
 * @LastEditors: 段丽军
 * @LastEditTime: 2023-12-27 14:33:55
 */
const enConfig = {
  type: "en",
  className: ["en"],
  layoutTop: {
    monitorDialogue: "Monitor Dialogue",
    logoutText: "Log out"
  },
  welcomeMessage: "Welcome to log in to the Qunfeng back-end management system",
  cancelText: "cancel",
  ConfirmText: "Confirm",
  TipText: "Tip",
  requestFailed: "Request failed",
  requestResponseTips: "Response error",
  operSuccess: "Success",
  operationCompleted: "Operation Completed",
  chat: {
    chatList: {
      current: "Current Conversation",
      end: "Ended Conversation",
      inputSearchPlaceholder: "Please Input Content",
      myChat: "My Conversations",
      robotChat: " ",
      takeoverError: "Failed to takeover",
      noConversationsAvailable: "No Conversations Available",
      noNicknameFound: "No nickname found",
      files: "files",
      images: "images",
      emojis: "emojis",
      form: "form",
      visitorInformation: "Ticket Submitted",
      noNewsAvailable: "No news Available",
      secondText: "s",
      minuteText: "min",
      hourText: "h",
      history: "history",
      waitText: "The user has been waiting for x minutes.",
      endTabBottomText: "Messages from the past 30 minutes or up to a maximum of 50 conversations have been retrieved. Please find more details in "
    },
    accountOffline: {
      accountOfflineText: "Account offline",
      btnText: "Log in again",
      contentText: "Account disconnected; The current account is logged in on the EasyChat client; Please log in again."
    },
    chatContent: {
      msgTimeLabel: "Yesterday",
      noDataTips: "Start a conversation by selecting a user from the left user list",
      seizeChatBtn: "Take over this conversation",
      transferBtn: "Transfer",
      endConversationBtn: "End Conversation",
      visitor: "Visitor",
      ai: "AI",
      agent: "Agent",
      tip: "Tip",
      transferTips: "After transferring the conversation, it will require manual reception, cannot be returned to the AI",
      expandBtn: "Expand",
      collapseBtn: "Collapse",
      leadNotificationResults: "Lead Notification Results: ",
      leadNotificationSuccess: "Lead Notification xxx-Success",
      leadNotificationFailed: "Lead Notification xxx-Failed",
      viewDetails: "View Details",
      inputSendPlaceholder: "Please input your inquiry...",
      emoji: "Emoji",
      image: "Image",
      sendAreaEndTips: "Cannot send messages after the conversation ends",
      sendAreaMonitoringTips: "Cannot send messages while monitoring the conversation",
      sendImageFormatTips: "Only .jpg, .jpeg, .png format images are supported",
      sendImageSizeTips: "Uploaded images cannot exceed 1MB",
      requestResponseTips: "Response error",
      imageUploadError: "Image upload error",
      emptyMsgTips: "Empty",
      sendLongTips: "The content cannot exceed 1000 characters, the current length is: ",
      sendBtn: "Send",
      notSupportMsgTips: "This message type is not currently supported, please log in to the client to view",
      loadingTips: "Loading",
      loadFailTips: "Load failed",
      visitorLeadTitle: "Lead",
      generalResponsesTitle: "Response",
      LeadFormRequiredTips: "Required",
      LeadSaveLeadBtn: "Save",
      LeadOptionUnknown: "Gender not specified",
      LeadOptionMan: "Male",
      LeadOptionWoman: "Female",
      LeadOperSuccess: "Success",
      LeadFormNotSupportFieldTips: "Not supported type",
      GeneralInputSearchPlaceholder: "Please input query content",
      GeneralSendBtn: "Send",
      GeneralLoadingTips: "Loading",
      GeneralNoMoreTips: "No more Info",
      loadMore: "Load More.",
      loadEndPre: "Show only the latest 100 messages. Check “",
      loadEndSuf: "”for more."
    }
  },
  messageNotification: {
    text: "Message Notification Settings",
    soundNotification: "Sound Notification",
    bubbleNotification: "Bubble Notification",
    youConversation: "You have a new conversation",
    youMessage: "You have a new message",
    colleagueConversation: "Colleague have a new conversation",
    colleagueMessage: "Colleague have a new message"
  },
  monitorDialogue: {
    title: "Agent Monitoring",
    inputSearchPlaceholder: "Search",
    deptDefault: "Company Name",
    notData: "Not Data",
    selectAll: "Select All",
    selected: "Selected",
    notSelectPlaceholder: "You  have not select any person yet",
    maxCountTips: " Exceeded the maximum value: ",
    btnCancel: "Cancel",
    btnSave: "Save"
  },
  aiRobot: {
    switchLabel: "Bot's Status:",

    uploadTitle: "Files uploaded",
    uploadTip: "Please upload files in text, PDF, Word, CSV, XLS, or XML formats only.\nYou may upload up to 15 files with a combined maximum storage of 100MB. \nIndividual files must not exceed this storage limit to ensure a successful upload",
    uploadBtn: "upload",

    uploadStatusOption1: "Upload completed",
    uploadStatusOption2: "Upload failed",
    uploadStatusOption2Tip: "Check your file and retry, or reach out for assistance",
    uploadOperBtn1: "View",
    uploadOperBtn2: "Delete",
    uploadOperBtn3: "Rename",
    uploadBatchOperBtn1: "Remove from recent list",
    uploadTableColumn1: "File name",
    uploadTableColumn2: "File size",
    uploadTableColumn3: "Date uploaded",
    uploadTableColumn4: "Status",
    uploadTableColumn5: "More actions",
    uploadOperBtn1TipTitle: "Rename",
    uploadOperBtn1Tip: "Please enter a new name for the item:",
    uploadOperBtn1Btn1: "Confirm",
    uploadOperBtn1Btn2: "Cancel",
    uploadOperBtn2TipTitle: "Delete file",
    uploadOperBtn2Tip: "Are you sure you want to delete this file?",
    uploadOperBtn2Btn1: "Delete",
    uploadOperBtn2Btn2: "Cancel",

    titleHome: "Home",
    optionDay: "Day",
    optionWeek: "Week",
    optionMonth: "Month",
    cardTitle1: "AI Chats total",
    cardTitle2: "Visitors",
    cardTitle3: "Engaged Visitors",
    cardTitle4: "Converted Visitors",
    cardTip1: "Date Range",
    cardTip2: "Up from",

    titleManage: "Manage Docs",
    updateTip: "URLs may be updated once per week. \nPlease review your changes thoroughly before applying an update.",
    updateBtn: "update",

    titleTest: "Test Bot"
  },
  status: {
    online: "Online",
    away: "Away",
    busy: "Busy"
  },
  serviceCode: {
    "90000": "Token not found or has expired.",
    "90001": "Token not found, access denied.",
    "90002": "Invalid token.",
    "90003": "Account doesn't have permission to access this application.",
    "90004": "Token authentication center validation failed.",
    "90005": "Account doesn't have permission to access this resource.",
    "90006": "Identity confirmation required.",
    "90010": "Unable to retrieve authenticated account.",
    "90011": "Unable to parse Product ID.",
    "401": "Invalid token",
    "403": "Invalid token"
  }
};

export default enConfig;