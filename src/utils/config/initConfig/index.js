/*
 * @Description:
 * @Author: chunxu.Zhao
 * @Date: 2023-12-01 17:50:06
 * @LastEditors: chunxu.Zhao
 * @LastEditTime: 2024-05-10 15:36:31
 * @FilePath: \hw-web-client-view\src\utils\config\initConfig\index.js
 */
import ELCONFIG from "@/utils/config/lang";

let uiDropdown = [
    {id: "userStatusOnline", name: ELCONFIG.status.online, icon: "icon-zaixianzhuangtai system-dropdown-online", disabled: false},
    {id: "userStatusLeave", name: ELCONFIG.status.away, icon: "icon-zaixianzhuangtai system-dropdown-leave", disabled: false},
    {id: "userStatusBusy", name: ELCONFIG.status.busy, icon: "icon-zaixianzhuangtai system-dropdown-busy", disabled: false}
];

let appConfig = {};
let storeKey = "";

export const getJumpContext = () => {
  if (window.isLocal) {
    return "";
  }
  const host = window.location.host;
  // if (host.indexOf("us-mgr") != -1) {
  //   return "agents";
  // }
  if (host.indexOf("im-api") != -1) {
    return "webcall";
  }
  return "webcall";
};



function exportData() {
  if (window.$CONFIG.lang == "cn") {
    uiDropdown = uiDropdown.concat({id: "bindPhone", name: "绑定手机号", icon: "icon-bangdingshoujihao"});
    appConfig = {
      appName: "抖推科技",
      welcomeMessage: "欢迎登录群峰后台管理系统",
      logoIconFont: "icon-doutuilogo",
      enAppName: "DOUTUI TECHNOLOGY",
      pageTitle: "",
      appId: 10000,
      jumpContext: "/webcall/", // /data-config/view/base
      packageName: "web-client-view"
    };
    storeKey = "web-client-view";
  } else if (window.$CONFIG.lang == "en") {
    // 注意 顺序不要打乱 有疑问找  因为有css 样式控制 改这里需要联动的改css
    uiDropdown = uiDropdown.concat({id: "messageSetting", name: ELCONFIG.messageNotification.text, icon: "icon-notification"});
    appConfig = {
      appName: "",
      welcomeMessage: ELCONFIG.welcomeMessage + process.env.VUE_APP_VERSION_HW, // 加这个是为了防止登录页的缓存 这个参数目前登录页没有用
      logoIconFont: "icon-easychator-logo",
      enAppName: " ",
      layoutText: ELCONFIG.layoutTop.logoutText,
      pageTitle: "upbeat",
      appId: 10000,
      jumpContext: `/${getJumpContext()}/`, // /data-config/view/base
      packageName: "hw-web-client-view",
      // avatarSrc: "https://nimg.ws.126.net/?url=http%3A%2F%2Fcms-bucket.ws.126.net%2F2023%2F1205%2Fcfbfebb8p00s566he0077c000ak0057c.png&thumbnail=453y225&quality=100&type=jpg"
      // avatarSrc: "https://nimg.ws.126.net/?url=http%3A%2F%2Fcms-bucket.ws.126.net%2F2023%2F1205%2F48f071f7j00s567iv009xc000550038c.jpg&thumbnail=185y116&quality=100&type=jpg"
    };
    storeKey= "hw-web-client-view";
  }
  return {
    appConfig,
    uiDropdown,
    storeKey
  };
}
export default exportData();