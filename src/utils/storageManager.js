/*
 * @Description:
 * @Author: 李大玄
 * @Date: 2023/11/1 10:31 AM
 * @FilePath: \web-client\src\utils\storageManager.js
 * @LastEditors: chunxu.Zhao
 * @LastEditTime: 2023-11-16 12:04:35
*/

import {alwaysTime, alwaysTimeMult} from "@/utils/alwaystime";

const storageType = window.sessionStorage;

function formatDate(date) {
  let options = { year: "numeric", month: "2-digit", day: "2-digit" };
  return date.toLocaleDateString("en-US", options);
}
export function getTodayAndYesterday() {
  let yesterday = new Date(Date.now() - 86400000);
  return {
    today: formatDate(new Date()),
    yesterday: formatDate(yesterday)
  };
}

const dates = getTodayAndYesterday();
// console.log("今天的日期：" + dates.today);
// console.log("昨天的日期：" + dates.yesterday);

class LocalStorageManag {
  constructor() {
    this.eventMsgOffset = "EventMsgOffset"; // 偏移量
    this.chatPrefix = "ChatMap"; // current end 存储对话列表的key
    this.recordChatMap = "RecordChatMap"; // 对话详情 {xxx: []} 对象集合
    this.chatIdToUserId = "chatIdToUserId"; // 对话id 对应 的userId
    this.firstFetchEndTime = "FirstFetchEndTime";
    this.baseLogin = "baseLogin";
    // this.isCreatAccount = "isCreatAccount";
    this.customerStatus = "customerStatus";
    this.messageRemindConfig = "messageRemindConfig";//消息提醒
    this.timeDiff = "timeDiff"; // 本地与服务器时间差
    this.grpupUnreadMessageMap = dates.today + "-grpupUnreadMessageMap"; // userID 分组级别的未读消息存储
    this.globalHaveUnreadMessage = dates.today + "-globalHaveUnreadMessage"; // userID 分组级别的未读消息存储
    this.unreadMessageCount = dates.today + "-unreadMessageCount"; // 未读消息数与未回复时间
    this.reckonTime = dates.today + "-reckonTime"; // 未读消息数与未回复时间
  }

  // 获取本地存储中的值
  get(key) {
    try {
      const value = storageType.getItem(key);
      if (value === null || value === undefined || value === "") {
        return null;
      }
      return JSON.parse(storageType.getItem(key));
    } catch (err) {
      return null;
    }
  }

  // 设置本地存储中的值
  set(key, value) {
    return storageType.setItem(key, JSON.stringify(value));
  }

  // 删除本地存储中的值
  remove(key) {
    return storageType.removeItem(key);
  }

  // 清空本地存储
  clear() {
    return storageType.clear();
  }

  // 获取 localsotrage 中以某个字符开头的所有数据
  getDataStartingWith(prefix) {
    let data = {};
    for (let key in storageType) {
      if (key.startsWith(prefix)) {
        data[key] = JSON.parse(storageType.getItem(key));
      }
    }
    return data;
  }
  removeDataStartingWith(prefix) {
    let data = {};
    for (let key in storageType) {
      if (key.startsWith(prefix)) {
        storageType.removeItem(key);
      }
    }
    return data;
  }
}

export const LocalStorageManager = new LocalStorageManag();

