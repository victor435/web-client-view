/*
 * @Description:
 * @Author: chunxu.Zhao
 * @Date: 2023-10-31 10:09:40
 * @LastEditors: chunxu.Zhao
 * @LastEditTime: 2024-05-14 14:56:06
 * @FilePath: \hw-web-client-view\src\utils\iframeSourceList.js
 */
const isEn = window.$CONFIG.lang == "en";
const appId = 10000;
// const token = "eyJhbGciOiJSUzI1NiJ9.eyJzdWIiOiLmmJPogYrnp5HmioAiLCJpc3MiOiLmmJPogYrnp5HmioAiLCJpYXQiOjE2OTg5MDUzMzYsImV4cCI6MTcwMTQ5NzMzNiwiY29tcGFueUlkIjo0MDAwOSwidXNlcklkIjoibWVudUFkbWluIiwiYXBwSWQiOiIxMDAwMCIsImRlcGFydG1lbnRJZCI6MTQ0MjgsInVzZXJLaWQiOjE5MDU5NywiZ2VuZXJhdGVUaW1lIjoiMjAyMy0xMS0wMiAxNDowODo1NiIsImp0aSI6IjQ0ZjliMDg1LTQ0NDEtNGU5Ny04NTcwLWFkNjEyZDJkMjRkNSJ9.BEVZUx4YjYBfvb16KY4wXRGyGujvansHuJGWkDWoKFyj0xoIzT2wQqrGVVRjTBCklbE7fdxCtgWlB7EWRRB-FZhBZHu5gQ68bPNtgQSW3KejCgEDyhRuHbpPhG2GL_vLofUcia_p5aaJKdpQY0ZaMI5KHIKO6XUl5U3k4UiIAPM";
const token = window.EWebPlat.store.getters.token;
const refreshToken = "xxx";
const clientFrom = isEn ? "hyWebClient" : "webClient";
const env = localStorage.getItem("env");
console.log(env, "env---");
// 后端返回的数据
// dev-prd19,test-prd18,prd19,group-prd3,group2-prd2,group-prd1

// 在线服务
let webcallOnlineDomainEn = {
  test: "//test19.easyliao.net",
  pre: "//pre.upbeat.chat",
  prod: "//mgr.upbeat.chat"
};
let webcallOnlineDomainCn = {
  "test-prd18": "//test-prd18.easyliao.net",
  "prd19": "//prd19.easyliao.com",
  "group2-prd4": "//group2-prd4-mgr.easyliao.com",
  "group-prd3": "//group-prd3-mgr.easyliao.com",
  "group2-prd2": "//group2-mgr.easyliao.com",
  "group-prd1": "//group-mgr.easyliao.com",
  "tx-mgr": "//tx-mgr.easyliao.com"
};

let webcallOnlineDomain = window.$CONFIG.lang == "cn" ? webcallOnlineDomainCn : webcallOnlineDomainEn;

const webcallOnlineMenus = ["pageCommonWord","customerAccount","channelQF", "chatWidget", "visitorCol","customerGroup","clues", "record", "HomeBusinessOverview", "almostAlwaysDate", "almostAlwaysArea","insertDate","insertGroup", "insertArea", "talkGroup", "talkReceptionType","busCardDate","serviceWork"];

// ai6
const ai6Domain = {
  test: "//test-prd18.easyliao.net/eachbot-flow-web/index",
  pre: "//prd19.easyliao.com/eachbot-flow-web/index",
  prod: "//train.easyliao.com/eachbot-flow-web/index"
};
const ai6Menus = ["pageMachineLearning", "pageKnowledgeBase"]; // "pageWordSearch"

// 用户中心
const userCenterMenus = isEn ? ["manual-user"] : ["department"]; // "role" "user" "department"
const userCenterDomain = {
  test: isEn ? "//test19.easyliao.net/auth-eupms/vi/ad" : "//test-prd18.easyliao.net/auth-eupms/vi/ad",
  pre: isEn ? "//pre-auth.upbeat.chat/auth-eupms/vi/ad" : "//pre-auth.easyliao.com/auth-eupms/vi/ad",
  prod: isEn ? "//auth.upbeat.chat/auth-eupms/vi/ad" : "//auth.easyliao.com/auth-eupms/vi/ad"
};
const uriMap = {
  'manual-user': "b7449c71-501a-4cae-962a-9a49eeb12d20",
  department: "992f506b-b189-4b35-9c01-9473104e9903",
  role: "6f7bec30-48b6-485c-9f97-e5deac278599",
  pageCommonWord: "633a9f19-fcdd-45f0-be56-3038e5254eed",
  visitorCol: "a3bc02db-970f-4a9c-9c32-a3883942cd73",
  chatWidget: "chatWidget",
  customerGroup: "2b2da17f-67fa-4321-ac11-92191a78e7d6",
  clues: "85cb2779-d129-4e3a-ac2d-d1724c1125fa",
  record: "chatRecord",
  HomeBusinessOverview: "report-home-business-profile",
  almostAlwaysDate: "35fd8b9f-b00f-4686-b0db-2c1d48c3415a",
  almostAlwaysArea: "fb916555-ef16-442d-a190-c90d1d6343b4",
  insertDate: "e3c6d7f1-ef87-48ae-b5cb-1e9896fbff99",
  insertGroup: "ecd5ef50-d8d8-44fc-a7c1-96b0de7ec83a",
  insertArea: "86ac1374-13ba-482e-a7ab-ddf60911f7df",
  talkGroup: "report_visitor_record_group",
  talkReceptionType: "report_visitor_record_receptionType",
  busCardDate: "cdfe1aed-8720-4253-8466-fe8b2e5e333c",
  serviceWork: "assess_orederCs"
};

const getSysList = (menus, system) => {
  const sources = [];
  for(let menu of menus) {
    const obj = new Object();
    let source = "";
    switch(system) {
      case "userCenter":
        source = `${userCenterDomain[window.__sso]}/${appId}/?code=${menu}&access_token=${token}&clientFrom=${clientFrom}${isEn?"&lang=en":""}`;
        break;
      case "ai6":
        source = `${ai6Domain[window.__sso]}/?access_token=${token}&refresh_token=${refreshToken}&clientFrom=${clientFrom}/#/${menu}`;
        break;
      case "webcallOnline":
        if (window.$CONFIG.lang == "cn") {
          if (menu === "pageCommonWord") {
            source = `${webcallOnlineDomain[env]}/console/?jumpType&clientFrom=${clientFrom}&check=${token}&pPath=auxliary&type=pageCommonWord`;
          } else if (menu === "customerAccount") {
            source = `${webcallOnlineDomain[env]}/console/?jumpType&clientFrom=${clientFrom}&check=${token}&pPath=configInfo&type=customerAccount`;
          } else if (menu === "customerGroup") {
            source = `${webcallOnlineDomain[env]}/console/?jumpType&clientFrom=${clientFrom}&check=${token}&pPath=configInfo&type=customerGroup`;
          } else if (menu === "record") {
            source = `${webcallOnlineDomain[env]}/record/?juliangCheck=${token}&clientFrom=${clientFrom}`;
          } else if (menu === "clues") {
            source = `${webcallOnlineDomain[env]}/clues/?juliangCheck=${token}&clientFrom=${clientFrom}`;
          } else if (menu === "channelQF") {
            source = `${webcallOnlineDomain[env]}/operate/?jumpType&check=${token}&clientFrom=${clientFrom}&type=channelQF`;
          }
        } else {
          if (menu === "pageCommonWord") {
            source = `${webcallOnlineDomain[window.__sso]}/console/?jumpType&clientFrom=${clientFrom}&check=${token}&pPath=auxliary&type=pageCommonWord`;
          } else if (menu === "visitorCol") {
            source = `${webcallOnlineDomain[window.__sso]}/console/?jumpType&clientFrom=${clientFrom}&check=${token}&pPath=auxliary&type=visitorCol`;
          }else if (menu === "record") {
            source = `${webcallOnlineDomain[window.__sso]}/record/?hwCheck=${token}`;
          } else if (menu === "clues") {
            source = `${webcallOnlineDomain[window.__sso]}/clues/?hwCheck=${token}`;
          } 
          // else if (menu === "chatWidget") {
          //   source = `${webcallOnlineDomain[window.__sso]}/console/?jumpType&clientFrom=${clientFrom}&check=${token}&pPath=style&type=chatWidget`;
          // } 
          // else if (menu === "customerGroup") {
          //   source = `${webcallOnlineDomain[window.__sso]}/console/?jumpType&clientFrom=${clientFrom}&check=${token}&pPath=configInfo&type=customerGroup`;
          // }
          //  else if (menu === "HomeBusinessOverview") {
          //   source = `${webcallOnlineDomain[window.__sso]}/report/?jumpType&check=${token}&clientFrom=${clientFrom}&pPath=index&type=homeBusinessOverview`;
          // } else if (menu === "almostAlwaysDate") {
          //   source = `${webcallOnlineDomain[window.__sso]}/report/?jumpType&check=${token}&clientFrom=${clientFrom}&pPath=almostAlways&type=almostAlwaysDate`;
          // }else if (menu === "almostAlwaysArea") {
          //   source = `${webcallOnlineDomain[window.__sso]}/report/?jumpType&check=${token}&clientFrom=${clientFrom}&pPath=almostAlways&type=almostAlwaysArea`;
          // }else if (menu === "insertDate") {
          //   source = `${webcallOnlineDomain[window.__sso]}/report/?jumpType&check=${token}&clientFrom=${clientFrom}&pPath=insert&type=insertDate`;
          // }else if (menu === "insertGroup") {
          //   source = `${webcallOnlineDomain[window.__sso]}/report/?jumpType&check=${token}&clientFrom=${clientFrom}&pPath=insert&type=insertGroup`;
          // }else if (menu === "insertArea") {
          //   source = `${webcallOnlineDomain[window.__sso]}/report/?jumpType&check=${token}&clientFrom=${clientFrom}&pPath=insert&type=insertArea`;
          // }else if (menu === "talkGroup") {
          //   source = `${webcallOnlineDomain[window.__sso]}/report/?jumpType&check=${token}&clientFrom=${clientFrom}&pPath=talk&type=talkGroup`;
          // }else if (menu === "talkReceptionType") {
          //   source = `${webcallOnlineDomain[window.__sso]}/report/?jumpType&check=${token}&clientFrom=${clientFrom}&pPath=talk&type=talkReceptionType`;
          // }else if (menu === "busCardDate") {
          //   source = `${webcallOnlineDomain[window.__sso]}/report/?jumpType&check=${token}&clientFrom=${clientFrom}&pPath=busCard&type=busCardDate`;
          // }else if (menu === "serviceWork") {
          //   source = `${webcallOnlineDomain[window.__sso]}/report/?jumpType&check=${token}&clientFrom=${clientFrom}&pPath=service&type=serviceWork`;
          // }
        }
        break;
      default:
        break;
    }
    obj.source = source; // iframe的src
    obj.params = menu; // 路由params /xxx/:id
    if(isEn) {
      obj.params = uriMap[menu];
    }
    sources.push(obj);
  }
  return sources;
};

export const sourceList = [
  ...getSysList(ai6Menus, "ai6"),
  ...getSysList(userCenterMenus, "userCenter"),
  ...getSysList(webcallOnlineMenus, "webcallOnline")
];