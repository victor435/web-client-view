// 在你的 Vue 2 项目中创建一个名为 "scroll-up" 的指令
export default {
  name: "scroll-up",
  bind(el, binding) {
    // 获取传递给指令的初始值
    let value = binding.value || 0;
    console.log("value", value);
    // 使用 MutationObserver 监听元素内容的变化
    const observer = new MutationObserver((mutationsList) => {
      console.log("mutationsList", mutationsList);
      if (mutationsList.length > 0) {
        // 创建一个滚动效果的动画
        const animation = el.animate(
            [{transform: `translateY(0px)`}, {transform: `translateY(-${value}px)`}],
            {duration: 1000, easing: 'ease-in-out'}
        );
        // 动画完成后将元素内容回到原始位置
        animation.onfinish = function () {
          el.style.transform = `translateY(0px)`;
        };
        // 更新 value 的值，用于下一次动画效果
        value += binding.value || 0;
      }
    });

    // 配置 MutationObserver 监听的选项
    const config = {childList: true,attributes: true,};

    // 开始监听元素内容的变化
    observer.observe(el, config);
  }
};
