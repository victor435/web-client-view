/*
 * @Description:
 * @Author: 李大玄
 * @Date: 2023/11/13 8:56 PM
 * @FilePath: src/api/eachbotFlowWeb.js
 * @LastEditors: 李大玄
 * @LastEditTime: 2023/11/13 8:56 PM
*/
export default {
  industryList: {
    url: "/industry/qunfeng",
    method: "get"
  },
  aiUserInit: {
    url: "/aiUser/init",
    method: "post"
  },
};