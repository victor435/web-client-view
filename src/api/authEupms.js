/*
 * @Description:
 * @Author: 李大玄
 * @Date: 2022-08-08 17:47:04
 * @FilePath:  src/api/authEupms.js
 * @LastEditors: 李大玄
 * @LastEditTime: 2022-08-10 09:46:00
 */

export default {
  getPerUserMenus: {
    url: "/per/user-menus/10000",
    method: "get"
  },
  configUpdate: {
    url: "/web/phone/bind",
    method: "post"
  },
  webPhoneSendcode: {
    url: "/web/phone/sendcode",
    method: "post"
  },
  webPhoneBindinfo: {
    url: "/web/phone/bindinfo",
    method: "post"
  },
  baseUserAiCreate: {
    url: "/base/user/ai/create",
    method: "post"
  },
  changeSite: {
    url: "/web/company/change/site",
    method: "post"
  },
  getCompanyInfo: {
    url: "/web/company/info",
    method: "get"
  },
  manualUserList: {
    url: "/base/user/manual-user-list",
    method: "get"
  },
  reservedUser: {
    url: "/base/user/reserved-user",
    method: "post"
  },
  // 获取AI用户信息
  aiUser: {
    url: "/base/user/ai-user",
    method: "get"
  }
};