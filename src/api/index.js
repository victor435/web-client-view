/*
 * @Description:
 * @Author: 李大玄
 * @Date: 2022-06-30 17:55:35
 * @FilePath: /web-framework-demo/src/api/index.js
 * @LastEditors: 李大玄
 * @LastEditTime: 2022-09-23 17:53:26
 */
import { contextEnum, proxyContext, requestheader } from "../application/baseUrl";
let ApiMap = {};
// 自动加载该目录下的所有文件
const files = require.context("./", true, /\.(vue|js)$/);
// 根据文件名组织模块对象
files.keys().map(src => {
  const match = src.match(/\/(.+)\./);
  if (match && match.length >= 1) {
    const name = match[1].split("/")[0];
    const moduleValue = files(src);
    if (moduleValue.default) {
      ApiMap[name] = { ...moduleValue.default };
    }
  }
});

const isDev = process.env.NODE_ENV == "development";
const newApiMap = {};
for (const fileKey in ApiMap) {
  const content = ApiMap[fileKey];
  const fileitem = {};
  for (const key in content) {
    const item = Object.assign({}, content[key], {
      type: content[key]["type"] || fileKey,
      url: contextEnum[fileKey] + content[key].url, // 评拼接上下文
      method: content[key].method || "get",
      requestheader: requestheader[fileKey]
    });

    if (isDev) {
      item.url = proxyContext[fileKey] + item.url;
    }
    fileitem[key] = item;
  }
  newApiMap[fileKey] = fileitem;
}
export default newApiMap;