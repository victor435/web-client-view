/*
 * @Description:
 * @Author: 李大玄
 * @Date: 2022-08-08 17:47:04
 * @FilePath:  src/api/authEupms.js
 * @LastEditors: 李大玄
 * @LastEditTime: 2022-08-10 09:46:00
 */

export default {
  accessToken: {
    url: "/oauth/token/refresh/access-token",
    method: "post"
  }
};