export default {
  apiReportHomeChart: {
    url: "/home/bar-chart",
    method: "post"
  },
  apiReportHomeCompareVisitor: {
    url: "/home/metrics-compare-visitor",
    method: "post"
  },
  apiReportHomeComparePv: {
    url: "/home/metrics-compare-pv",
    method: "post"
  },
  apiReportHomeCompareInteract: {
    url: "/home/metrics-compare-interact",
    method: "post"
  },
  apiReportHomePage: {
    url: "/home/page",
    method: "post"
  },
  apiReportHomeExport: {
    url: "/home/export-page",
    method: "post"
  },
};