/*
 * @Description:
 * @Author: 李大玄
 * @Date: 2022-07-29 16:06:06
 * @FilePath: /data-config-view/src/api/platCommon.js
 * @LastEditors: 李大玄
 * @LastEditTime: 2022-08-22 16:31:56
 */
export default {
  getMenuList: {
    method: "get",
    url: "/menu/list"
  },
  getUserInfo: {
    url: "/user/info",
    method: "get"
  },
  // 列表
  developPlanList: {
    url: "/develop-plan/page-list",
    method: "get"
  },
  // 新增
  developPlanAdd: {
    url: "/develop-plan/insert",
    method: "post"
  },
  // 编辑
  developPlanEdit: {
    url: "/develop-plan/update",
    method: "post"
  },
  // 删除
  developPlanRemove: {
    url: "/develop-plan/delete",
    method: "delete"
  },
  // 详情
  developPlanDetail: {
    url: "/develop-plan/details",
    method: "get"
  },
  // 任务详情
  developPlanPersonnelDetail: {
    url: "/task/query-by-project",
    method: "post"
  },
  // 产研用户-选项数据
  personnelOptions: {
    url: "/system-mapping/all-user",
    method: "get"
  }
};
