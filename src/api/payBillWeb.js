export default {
  // 产品明细
  detail: {
    url: "/stripe/getProduct",
    method: "get"
  },
  // 订单状态
  status: {
    url: "/stripe/getSubscriptionStatus",
    method: "get"
  },
  // 企业信息
  compnayInfo: {
    url: "/stripe/getCompanyAndProductConfig",
    method: "get"
  },
  // 账单明细
  subscriptionList: {
    url: "/stripeSubscription/subscriptionList",
    method: "get"
  }
};