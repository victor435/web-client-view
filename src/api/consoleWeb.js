export default {
  newStyleConfigDetail: {
    url: "/new-jsconfig/detail",
    method: "post"
  },
  // 新版网页样式保存
  newStyleConfigSave: {
    url: 'new-jsconfig/add',
    method: 'post',
  },
  // 新版网页样式编辑
  newStyleConfigUpdate: {
    url: 'new-jsconfig/update',
    method: 'post',
  },
  jsConfigPageJsConfig:{
    url:'/JsConfig/PageJsConfig',
    method: 'post',
  },
  getWelcomePage1:{
    url:'HomePage/WelcomePage1',
    method: 'get',
  },
  uploadPictureHw: {
    url: 'material/overseas/library/upload',
    method: 'post',
  }
};