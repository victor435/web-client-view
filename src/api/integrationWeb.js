/*
 * @Description: 
 * @Author: chunxu.Zhao
 * @Date: 2024-04-07 16:25:13
 * @LastEditors: chunxu.Zhao
 * @LastEditTime: 2024-05-10 17:31:17
 * @FilePath: \hw-web-client-view\src\api\integrationWeb.js
 */
export default {
  // 获取列表
  getAppStoreList: {
    url: "/app/store/list",
    method: "post"
  },
  // 打开 获取跳转url
  getZendeskAuthorizationUrl: {
    url: "/oauth/$1/auth-url/create",
    method: "post"
  },
  // 关闭
  unbindZendeskAuthorization: {
    url: "/app/store/unbindAuthorization",
    method: "get"
  },
};