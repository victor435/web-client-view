export default {
  apiTrustClient: {
    url: "/api/trust/client",
    method: "post"
  },
};