export default {
  // 文件列表
  aiFileList: {
    url: "/companyAiFiles/aiFileList",
    method: "post"
  },
  // 文件上传前校验
  checkAiFile: {
    url: "/companyAiFiles/checkAiFile",
    method: "post"
  },
  // 文件上传
  uploadAiFile: {
    url: "/companyAiFiles/uploadAiFile",
    method: "post"
  },
  // 文件重命名
  updateAiFileName: {
    url: "/companyAiFiles/updateAiFileName",
    method: "post"
  },
  // 文件删除
  aiFileDelete: {
    url: "/companyAiFiles/aiFileDelete",
    method: "post"
  }
};