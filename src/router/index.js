/*
 * @Description: router
 * @Author: 李大玄
 * @Date: 2022-07-18 22:07:33
 * @FilePath: \web-client\src\router\index.js
 * @LastEditors: chunxu.Zhao
 * @LastEditTime: 2023-10-31 18:36:40
 */
export const children = [
  {
    path: "/chat",
    component: () => import( /*webpackChunkName: "chat"*/ "../views/chat"),
    meta: {pageTitle: "Live Chat"}
  },
  {
    path: "/homeIndex/:id", meta: {keepAlive: !true, pageTitle: "History"},
    component: () => import( /*webpackChunkName: "homeIndex"*/ "../views/iframeList")
  },
  {
    path: "/chatRecord", meta: {keepAlive: true},
    component: () => import( /*webpackChunkName: "chatRecord"*/ "../views/comPagesEntry")
  },
  {
    path: "/console", meta: {keepAlive: true, pageTitle: "Settings"},
    component: () => import( /*webpackChunkName: "console"*/ "../views/comPagesEntry")
  },
  {
    path: "/aiRobot2", meta: {keepAlive: true, pageTitle: "AI Robot"},
    component: () => import( /*webpackChunkName: "aiRobot"*/ "../views/aiRobot")
  },
  {
    path: "/report", meta: {fullScreen: !true, keepAlive: !true, pageTitle: "Analytics"},
    component: () => import( /*webpackChunkName: "report"*/ "../views/report")
  },
  {
    path: "/chat/error",
    component: () => import( /*webpackChunkName: "home"*/ "../views/error.vue"),
    meta: {fullScreen: true, keepAlive: true}
  },
  {
    path: "/chat/industry",
    component: () => import( /*webpackChunkName: "home"*/ "../views/industry.vue"),
    meta: {fullScreen: true, keepAlive: true}
  }
];
export const routes = [
  {
    path: "/",
    name: "index",
    // component: () => import( /*webpackChunkName: "entry"*/ "ui@/components/layouts"),
    // component: () => import( /*webpackChunkName: "entry"*/ "ui@/components/newLayouts"),
    component: () => import( /*webpackChunkName: "entry"*/ "../views/layout.vue"),
    children: children
  },
  {
    path: "/chat/chatPage",
    component: () => import( /*webpackChunkName: "chat"*/ "../views/chat"),
    meta: {pageTitle: "Live Chat",fullScreen: true}
  }
];