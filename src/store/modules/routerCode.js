/*
 * @Description: 举例
 * @Author: 李大玄
 * @Date: 2022-06-22 18:17:24
 * @FilePath: /data-config-view/src/store/modules/test.js
 * @LastEditors: 李大玄
 * @LastEditTime: 2022-07-21 20:30:12
 */

export default {
  state: {
    code: ""
  },
  mutations: {
    SET_CODE: (state, val) => {
      state.code = val;
    }
  },
  actions: {
    setCode({commit}, val = "") {
      commit("SET_CODE", val);
    }
  }
};

