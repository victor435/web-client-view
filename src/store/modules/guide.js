/*
 * @Description:
 * @Author: chunxu.Zhao
 * @Date: 2023-11-03 11:04:46
 * @LastEditors: chunxu.Zhao
 * @LastEditTime: 2023-11-14 19:32:06
 * @FilePath: \web-client\src\store\modules\guide.js
 */
export default {
  state: {
    guideShow: false
  },
  mutations: {
    SET_GUIDE_SHOW: (state, data) => {
      state.guideShow = data;
    }
  },
  actions: {
    setGuideShow({ commit }, data = false) {
      commit("SET_GUIDE_SHOW", data);
    }
  }
};
