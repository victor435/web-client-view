/*
 * @Description:
 * @Author: 李大玄
 * @Date: 2023/11/1 10:14 AM
 * @FilePath: src/store/modules/chat.js
 * @LastEditors: 李大玄
 * @LastEditTime: 2023/11/1 10:14 AM
*/

export default {
  state: {
    chatMap: {},
    chatRecord: {},
    eventMsgOffset: 0
  },
  mutations: {
    SET_CHAT_MAP: (state, data) => {
      state.chatMap[data.key] = data.value;
      state.chatMap = Object.assign({}, state.chatMap);
      // state.chatMap = data;
    },
    // 对话详情
    SET_CHAT_RECORD: (state, data) => {
      /* 追加新的数据 */
      if (data.type == "add") {
        state.chatRecord[data.key] = data.value;
        state.chatRecord = Object.assign({}, state.chatRecord);
      } else if (data.type == "cover") {
        /* 覆盖原有数据 因为可能会删除掉部分数据 */
        state.chatRecord = Object.assign({}, data);
      }
      // state.chatMap = data;
    },
    SET_EVENTMSG_OFFSET: (state, data) => {
      state.eventMsgOffset =data;
    }
  },
  actions: {
    setChatMap({commit}, data = {}) {
      commit("SET_CHAT_MAP", data);
    },
    // 对话详情
    setChatRecord({commit}, data = {}) {
      commit("SET_CHAT_RECORD", data);
    },
    setEventMsgOffset({commit}, data) {
      commit("SET_EVENTMSG_OFFSET", data);
    }
  }
};

