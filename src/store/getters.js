/*
 * @Description: getters
 * @Author: 李大玄
 * @Date: 2022-06-22 18:17:24
 * @FilePath: \web-client\src\store\getters.js
 * @LastEditors: chunxu.Zhao
 * @LastEditTime: 2023-11-03 11:24:57
 */
const gettersTest = {
  num: (state) => state.test.num
};

const gettersChat = {
  localChatMap: (state) => state.chat.chatMap,
  chatRecord: (state) => state.chat.chatRecord,
  eventMsgOffset: (state) => state.chat.eventMsgOffset
};
const gettersGuide = {
  isGuideShow: state => state.guide.guideShow
};

const gettersCode = {
  routerCode: state => state.routerCode.code
};

export default { ...gettersTest, ...gettersChat, ...gettersGuide, ...gettersCode };
