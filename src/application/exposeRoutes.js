/*
 * @Description:
 * @Author: 李大玄
 * @Date: 2022-07-28 10:26:03
 * @FilePath: /data-config-view/src/application/exposeRoutes.js
 * @LastEditors: 李大玄
 * @LastEditTime: 2022-08-19 10:59:37
 */
import config from "../../package.json";
__webpack_public_path__ = `${process.env.VUE_APP_Resource_Address}/${config.name}/lib/`;
import "./commonExport";
import { children as routes } from "@/router";
import apiMap from "@/api";
import { apiConfig, iconLinkArr } from "./baseUrl";

var MoudleA = {
  init(plat) {
    plat.addMoudleRoutes(routes);
  }
};

if (EWebPlat && EWebPlat.registerMoudle) {
  EWebPlat.addMoudleService({ apiConfig, apiMap });
  EWebPlat.registerMoudle(MoudleA);
}