/*
 * @Description:
 * @Author: 李大玄
 * @Date: 2022-08-16 14:19:37
 * @FilePath: \hw-web-client-view\src\application\enums.js
 * @LastEditors: chunxu.Zhao
 * @LastEditTime: 2024-04-03 18:15:28
 */
import uuid from "uuid";
import _ from "lodash";
import {LocalStorageManager} from "@/utils/storageManager";

const chatList = "chat"; // 对话列表
const record = "chatRecord"; // 消息记录
const clues = "85cb2779-d129-4e3a-ac2d-d1724c1125fa"; // 名片管理

const analytics = "report"; // 数据报表
const home = "report-page-home"; // 首页
const overview = "report-home-business-profile"; // 业务概况
const summary = "6a49339d-94ab-4e29-8ca6-dac08548c64c"; // 概总
const summaryStatisticsByDate = "35fd8b9f-b00f-4686-b0db-2c1d48c3415a"; // 按日期统计
const summaryStatisticsByTime = "fb916555-ef16-442d-a190-c90d1d6343b4"; // 按地区统计

const accessAnalysis = "6a49339d-94ab-4e29-8ca6-dac08548c64d"; // 接入分析
const accessAnalysisByTime = "e3c6d7f1-ef87-48ae-b5cb-1e9896fbff99"; // 接入分析 按日期统计
const accessAnalysisByGroup = "ecd5ef50-d8d8-44fc-a7c1-96b0de7ec83a"; // 接入分析 按分组统计
const accessAnalysisByAddres = "86ac1374-13ba-482e-a7ab-ddf60911f7df"; // 接入分析 按省市统计

const conversationAnalysis = "report_visitor_record"; // 对话分析
const conversationAnalysisByGroup = "report_visitor_record_group"; // 对话分析 按分组统计
const conversationAnalysisByReceptionType = "report_visitor_record_receptionType"; // 对话分析按接待类型统计

const leadAnalysis = "6a49339d-94ab-4e29-8ca6-dac08548c67"; // 名片分析
const leadAnalysisByTime = "cdfe1aed-8720-4253-8466-fe8b2e5e333c"; // 名片分析 按日期统计

const agentAssessment = "6b38fac3-476c-42e1-8dc7-50c6a91eed82"; // 客服考核
const customerServicePerformanceAssessment = "assess_orederCs"; // 客服考核 客服工作情况考核

const settings = "console"; // 设置中心
const accountMgmt = "b7449c71-501a-4cae-962a-9a49eeb12d20"; // 账号管理
const roleMgmt = "6f7bec30-48b6-485c-9f97-e5deac278599"; // 角色管理
const departmentMgmt = "992f506b-b189-4b35-9c01-9473104e9903"; // 部内管理
// const customerGrpMgmt = "2b2da17f-67fa-4321-ac11-92191a78e7d6"; // 客户分组管理
const responseGrpMgmt = "633a9f19-fcdd-45f0-be56-3038e5254eed"; // 常用语管理
const leadFieldsMgmt = "a3bc02db-970f-4a9c-9c32-a3883942cd73"; // 名片字段管理
const chatWidget = "chatWidget"; // 网页样式配置 新版 code 码需要改动
const billing = "billing"; // 订阅页面
const integration = "integration"; // Integration

const chatMonitor = "chatMonitor"; // 监控者
const transchat = "transchat"; // 对话转移
const seizeChat = "seizeChat"; // 抢接对话

const aiRobot = "aiRobot2"; // uid
const aiRobotHome = "aiRobotHome"; // uid
const aiRobotManageDocs = "aiRobotManageDocs"; // uid
const aiRobotTestBot = "aiRobotTestBot"; // uid

const mapKey = {
  [chatList]: "Live Chat", // 对话列表
  [record]: "History", // 消息记录
  [clues]: "Lead", // 名片管理

  [analytics]: "Analytics", // 数据报表
  [home]: "Home", // 首页
  [overview]: "Overview", // 业务概况

  [summary]: "Summary", // 概总
  [summaryStatisticsByDate]: "Date", // 按日期统计
  [summaryStatisticsByTime]: "Time", // 按地区统计

  [accessAnalysis]: "Access", // 接入分析
  [accessAnalysisByTime]: "Date", // 接入分析 按日期统计  --------------------------------
  [accessAnalysisByGroup]: "Group", // 接入分析 按分组统计  --------------------------------
  [accessAnalysisByAddres]: "Region", // 接入分析 按省市(地区)统计  --------------------------------

  [conversationAnalysis]: "Dialogue", // 对话分析
  [conversationAnalysisByGroup]: "Group", // 对话分析 按分组统计
  [conversationAnalysisByReceptionType]: "Reception", // 对话分析 按接待类型统计  --------------------------------

  [leadAnalysis]: "Lead", // 名片分析
  [leadAnalysisByTime]: "Date", // 名片分析   按日期统计  --------------------------------

  [agentAssessment]: "Assessment", // 客服考核
  [customerServicePerformanceAssessment]: "Agent", // 客服考核 客服工作情况考核  --------------------------------

  [settings]: "Settings", // 设置中心
  [accountMgmt]: "Roles", // 账号管理Account
  [roleMgmt]: "Role", // 角色管理
  [departmentMgmt]: "Department", // 部内管理
  // [customerGrpMgmt]: "Group", // 客户分组管理
  [responseGrpMgmt]: "Response", // 常用语管理
  [leadFieldsMgmt]: "Field", // 名片字段管理
  [chatWidget]: "Chat widget", // 网页样式配置
  [billing]: "Billing", // 订阅页面
  [integration]: "Integration", // Integration
  // "AI 设置中心": "AI Settings", // AI 设置中心
  // "机器人管理": "AI", // 机器人管理
  // "知识库管理": "Archive", // 知识库管理
  [aiRobot]: "AI Robot", //
  [aiRobotHome]: "Home", //
  [aiRobotManageDocs]: "Manage Docs", //
  [aiRobotTestBot]: "Test Bot" //
};

function processObject(obj) {
  for (let key in obj) {
    if (obj[key].children && obj[key].children.length) {
      obj[key].children = processObject(obj[key].children);
    }
    obj[key].name = mapKey[obj[key].code];
  }
  return obj;
}

export function fotmatterEnum(list) {
  let menuObj = {};
  console.log("list", list);
  list.forEach(ele => {
    const settingArr = [accountMgmt, roleMgmt, departmentMgmt, responseGrpMgmt, leadFieldsMgmt, billing, integration];
    if ([chatList, analytics, settings, aiRobot].includes(ele.uid)) {
      ele.uri = ele.uid;
    } else if ([record].includes(ele.uid)) {
      ele.uri = "homeIndex/" + ele.uid;
    // } else if (clues.includes(ele.uid)) { //   名片
      // ele.uri = "chat/" + ele.uid;
    }
    ele.code = ele.uid;
    ele.iconUrl = "";
    menuObj[ele.uid] = ele;
  });
  const homeChildren = _.compact([menuObj[overview]/*业务概况*/]); // 首页子级
  const summaryChildren = _.compact([menuObj[summaryStatisticsByDate], /*按日期统计*/menuObj[summaryStatisticsByTime]]); // 概总子级
  const accessAnalysisChildren = _.compact([menuObj[accessAnalysisByTime], /*按日期统计*/menuObj[accessAnalysisByGroup], /*按分组统计*/menuObj[accessAnalysisByAddres]]); // 接入分析子级
  const conversationAnalysisChildren = _.compact([menuObj[conversationAnalysisByGroup], /*按分组统计*/menuObj[conversationAnalysisByReceptionType]]); // 对话分析子级
  const leadAnalysisChildren = _.compact([menuObj[leadAnalysisByTime]]); // 名片分析子级
  const agentAssessmentChildren = _.compact([menuObj[customerServicePerformanceAssessment]]); // 客服考核子级
  const analyticsChildren = _.compact([
    homeChildren.length ? Object.assign({}, menuObj[home] /*首页*/, {children: homeChildren}) : "",
    summaryChildren.length ? Object.assign({}, menuObj[summary] /*概总*/, {children: summaryChildren}) : "",
    accessAnalysisChildren.length ? Object.assign({}, menuObj[accessAnalysis] /*接入分析*/, {children: accessAnalysisChildren}) : "",
    conversationAnalysisChildren.length ? Object.assign({}, menuObj[conversationAnalysis] /*对话分析*/, {children: conversationAnalysisChildren}) : "",
    leadAnalysisChildren.length ? Object.assign({}, menuObj[leadAnalysis] /*名片分析*/, {children: leadAnalysisChildren}) : "",
    agentAssessmentChildren.length ? Object.assign({}, menuObj[agentAssessment] /*客服考核*/, {children: agentAssessmentChildren}) : ""
  ]); // 数据报表子级
  const consoleChildren = _.compact([
    menuObj[accountMgmt], // 客服账号管理
    // menuObj[roleMgmt], // 公司角色管理
    // menuObj[departmentMgmt], // 部门信息管理
    // menuObj[customerGrpMgmt], // 客服分组管理
    menuObj[responseGrpMgmt], // 公司常用语
    menuObj[leadFieldsMgmt], //名片字段管理
    menuObj[chatWidget], // 网页版配置
    menuObj[billing], // 订阅页面
    menuObj[integration], // Integration
  ]);
  const aiRobotChildren = _.compact([
    menuObj[aiRobotHome], // 首页
    menuObj[aiRobotManageDocs], //
    menuObj[aiRobotTestBot] // 机器人测试
  ]);
  LocalStorageManager.set("otherPromise", {chatMonitor: menuObj[chatMonitor], seizeChat: menuObj[seizeChat]});
  let newData = [
    menuObj[chatList] ? Object.assign({}, menuObj[chatList], {iconUrl: "icon-yiji-duihualiebiao"}) : "", // 对话列表 icon-yiji-duihualiebiao
    menuObj[record] ? Object.assign({}, menuObj[record], {iconUrl: "icon-yiji-xiaoxijilu"}) : "", // 消息记录 icon-yiji-xiaoxijilu
    // menuObj[clues] ? Object.assign({}, menuObj[clues], {iconUrl: "icon-yiji-mingpianguanli"}) : "", // 名片管理
    menuObj[analytics] ? Object.assign({}, menuObj[analytics] /*数据报表*/, {iconUrl: "icon-report2"}, analyticsChildren.length ? {children: analyticsChildren} : {}) : "",
    menuObj[settings] ? Object.assign({}, menuObj[settings] /*设置中心*/, {iconUrl: "icon-Settings"}, consoleChildren.length ? {children: consoleChildren} : {}) : "",
    menuObj[aiRobot] ? Object.assign({}, menuObj[aiRobot] /*AI机器人*/, {iconUrl: "icon-AI"}, aiRobotChildren.length ? {children: aiRobotChildren} : {}) : ""
  ];
  const materData = processObject(_.compact(newData));

  LocalStorageManager.set("enumsOriginData", materData);
  const showData = _.cloneDeep(materData).map(item => {
    if (item.children && item.children.length) {
      if (["homeIndex/console"].includes(item.uri)) {
        // item.uri = getFirstUri(item);
      }
      delete item.children;
    }
    return item;
  });
  return showData;
}


export let menus = [
  {
    "id": uuid(),
    "code": "chat",
    "productId": "10000",
    "name": "对话列表",
    "uri": "chat",
    "score": 1,
    "iconUrl": "icon-yiji-duihualiebiao",
    "type": 1
  },
  {
    "id": uuid(),
    "code": "homeIndex/record",
    "productId": "10000",
    "name": "消息记录",
    "uri": "homeIndex/record",
    "score": 1,
    "iconUrl": "icon-yiji-xiaoxijilu",
    "type": 1
  },
  {
    "id": uuid(),
    "code": "homeIndex/clues",
    "productId": "10000",
    "name": "名片管理",
    "uri": "homeIndex/clues",
    "score": 1,
    "iconUrl": "icon-yiji-mingpianguanli",
    "type": 1
  },
  {
    "id": 27380,
    "code": "homeIndex/clues",
    "productId": "10000",
    "name": "数据报表",
    "uri": "homeIndex/clues",
    "score": 1,
    "iconUrl": "icon-yiji-mingpianguanli",
    "type": 1,
    "children": [
      {
        "id": "173811",
        "code": "config",
        "productId": "10000",
        "name": "首页",
        "uri": "homeIndex/HomeBusinessOverview",
        "score": 1,
        "iconUrl": null,
        "type": 10,
        "children": [{
          "id": "173811",
          "code": "config",
          "productId": "10000",
          "name": "业务概况",
          "uri": "homeIndex/HomeBusinessOverview",
          "score": 1,
          "iconUrl": null,
          "type": 10,
          "children": []
        }]
      },
      {
        "id": uuid(),
        "code": "config",
        "productId": "10000",
        "name": "概总",
        "uri": "homeIndex/almostAlwaysDate",
        "score": 1,
        "iconUrl": null,
        "type": 10,
        "children": [{
          "id": uuid(),
          "code": "config",
          "productId": "10000",
          "name": "按日期统计",
          "uri": "homeIndex/almostAlwaysDate",
          "score": 1,
          "iconUrl": null,
          "type": 10,
          "children": []
        }, {
          "id": uuid(),
          "code": "config",
          "productId": "10000",
          "name": "按地区统计",
          "uri": "homeIndex/almostAlwaysArea",
          "score": 1,
          "iconUrl": null,
          "type": 10,
          "children": []
        }]
      },
      {
        "id": uuid(),
        "code": "config",
        "productId": "10000",
        "name": "接入分析",
        "uri": "homeIndex/insertDate",
        "score": 1,
        "iconUrl": null,
        "type": 10,
        "children": [{
          "id": uuid(),
          "code": "config",
          "productId": "10000",
          "name": "按日期统计",
          "uri": "homeIndex/insertDate",
          "score": 1,
          "iconUrl": null,
          "type": 10,
          "children": []
        }, {
          "id": uuid(),
          "code": "config",
          "productId": "10000",
          "name": "按分组统计",
          "uri": "homeIndex/insertGroup",
          "score": 1,
          "iconUrl": null,
          "type": 10,
          "children": []
        }, {
          "id": uuid(),
          "code": "config",
          "productId": "10000",
          "name": "按省市统计",
          "uri": "homeIndex/insertArea",
          "score": 1,
          "iconUrl": null,
          "type": 10,
          "children": []
        }]
      },
      {
        "id": uuid(),
        "code": "config",
        "productId": "10000",
        "name": "对话分析",
        "uri": "homeIndex/talkGroup",
        "score": 1,
        "iconUrl": null,
        "type": 10,
        "children": [{
          "id": uuid(),
          "code": "config",
          "productId": "10000",
          "name": "按分组统计",
          "uri": "homeIndex/talkGroup",
          "score": 1,
          "iconUrl": null,
          "type": 10,
          "children": []
        }, {
          "id": uuid(),
          "code": "config",
          "productId": "10000",
          "name": "按接待类型统计",
          "uri": "homeIndex/talkReceptionType",
          "score": 1,
          "iconUrl": null,
          "type": 10,
          "children": []
        }]
      },
      {
        "id": uuid(),
        "code": "config",
        "productId": "10000",
        "name": "名片分析",
        "uri": "homeIndex/busCardDate",
        "score": 1,
        "iconUrl": null,
        "type": 10,
        "children": [{
          "id": uuid(),
          "code": "config",
          "productId": "10000",
          "name": "按日期统计",
          "uri": "homeIndex/busCardDate",
          "score": 1,
          "iconUrl": null,
          "type": 10,
          "children": []
        }]
      },
      {
        "id": uuid(),
        "code": "config",
        "productId": "10000",
        "name": "客服考核",
        "uri": "homeIndex/serviceWork",
        "score": 1,
        "iconUrl": null,
        "type": 10,
        "children": [{
          "id": uuid(),
          "code": "config",
          "productId": "10000",
          "name": "客服工作情况考核",
          "uri": "homeIndex/serviceWork",
          "score": 1,
          "iconUrl": null,
          "type": 10,
          "children": []
        }]
      }
    ]
  },
  {
    "id": 17381,
    "code": "config",
    "productId": "10000",
    "name": "设置中心",
    "uri": "config",
    "score": 1,
    "iconUrl": "icon-yiji-shezhizhongxin",
    "type": 1,
    "children": [
      {
        "id": "173814",
        "code": "config",
        "productId": "10000",
        "name": "账号管理",
        "uri": "homeIndex/user",
        "score": 2,
        "iconUrl": "",
        "type": 10,
        "children": []
      }, {
        "id": "1738113",
        "code": "config",
        "productId": "10000",
        "name": "角色管理",
        "uri": "homeIndex/role",
        "score": 1,
        "iconUrl": null,
        "type": 10,
        "children": []
      }, {
        "id": "1738117",
        "code": "config",
        "productId": "10000",
        "name": "部门管理",
        "uri": "homeIndex/department",
        "score": 1,
        "iconUrl": null,
        "type": 10,
        "children": []
      },
      {
        "id": uuid(),
        "code": "config",
        "productId": "10000",
        "name": "客服分组管理",
        "uri": "homeIndex/customerGroup",
        "score": 2,
        "iconUrl": "",
        "type": 10,
        "children": []
      },
      {
        "id": uuid(),
        "code": "config",
        "productId": "99009",
        "name": "常用语设置",
        "uri": "homeIndex/pageCommonWord",
        "score": 2,
        "iconUrl": "",
        "type": 10,
        "children": []
      }, {
        "id": uuid(),
        "code": "config",
        "productId": "99009",
        "name": "名片字段管理",
        "uri": "homeIndex/visitorCol",
        "score": 2,
        "iconUrl": "",
        "type": 10,
        "children": []
      }, {
        "id": uuid(),
        "code": "config",
        "productId": "99009",
        "name": "网页样式配置",
        "uri": "homeIndex/chatWidget",
        "score": 2,
        "iconUrl": "",
        "type": 10,
        "children": []
      }, {
        "id": uuid(),
        "code": "config",
        "productId": "99009",
        "name": "订阅页面",
        "uri": "homeIndex/billing",
        "score": 2,
        "iconUrl": "",
        "type": 10,
        "children": []
      }
    ]
  }
];
