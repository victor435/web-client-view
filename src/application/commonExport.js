/*
 * @Description:
 * @Author: 李大玄
 * @Date: 2022-08-15 10:25:56
 * @FilePath: /web-framework-demo/src/application/registerComponents.js
 * @LastEditors: 李大玄
 * @LastEditTime: 2022-10-27 15:46:55
 */

import Vue from "vue";
import apiMap from "@/api";
import Pl from "../components/pl.vue";
/*
plat.platService(ucenterUsermanageApiMap.authEupms.getSites, this.params)
*/
window["webClientApiMap"] = apiMap;

(function registerComponents() {
  Vue.component("Pl", Pl);
})();