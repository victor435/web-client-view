/*
 * @Description:
 * @Author: chunxu.Zhao
 * @Date: 2023-11-14 11:22:30
 * @LastEditors: chunxu.Zhao
 * @LastEditTime: 2024-04-12 14:58:06
 * @FilePath: \hw-web-client-view\src\application\statusData.js
 */
import {LocalStorageManager} from "@/utils/storageManager";
import ELEMENTUI from "element-ui";
function showMessage(type = "success", msg = "") {
  if (msg) {
    ELEMENTUI.Message[type](msg);
  }
//  showMessage("error", res.msg || "请求失败");
}
export const stateList = {
  1: "userStatusOnline",
  0: "userStatusOnline2",
  2: "userStatusBusy",
  3: "userStatusLeave"
};
export const setStatusColor = (state) => {
  const ulDom = document.querySelector(".popoverCard");
  for(let item of ulDom.childNodes) {
    if(item.innerText.includes(window.ELCONFIG.status.online)) {
      if(state == 0 || state == 1) {
        item.classList.add("active_state");
      } else {
        item.classList.remove("active_state");
      }
    } else if(item.innerText.includes(window.ELCONFIG.status.busy)) {
      if(state == 2) {
        item.classList.add("active_state");
      } else {
        item.classList.remove("active_state");
      }
    } else if(item.innerText.includes(window.ELCONFIG.status.away)) {
      if(state == 3) {
        item.classList.add("active_state");
      } else {
        item.classList.remove("active_state");
      }
    }
  }
  LocalStorageManager.set(LocalStorageManager.customerStatus, state);
};
export const getStatus = (state) => {
  const btnDom = document.querySelector(".avatar-box");
  btnDom.classList.add("customer_state");
  for(let [i, c] of Object.entries(stateList)) {
    if(i == state) {
      btnDom.classList.add(c);
    } else {
      btnDom.classList.remove(c);
    }
  }
  setStatusColor(state);
};
// 进来后获取登录状态
export const getUserStatus = async (callback) => {
  let status = 1;
  const res2 = await window.EWebPlat.platService(window.webClientApiMap.clientPlatform.getUserInfo);
  if(res2.code == 0) {
    status = res2.data?.runningStatus
  }
  LocalStorageManager.set(LocalStorageManager.customerStatus, status);
  const res = await EWebPlat.platService(webClientApiMap.clientPlatform.userLogin, {
    clientType: "WebClientServer",
    status: Number(status)
  });
  if (res.code) {
    showMessage("error", res.msg || ELCONFIG.requestFailed);
    return;
  }
  setTimeout(() => {
    if(document.querySelector(".avatar-box")) {
      getStatus(Number(status)); // res.data.runningStatus
    } else {
      const timer = setInterval(() => {
        if(document.querySelector(".avatar-box")) {
          getStatus(Number(status));
          clearInterval(timer);
        }
      }, 300);
    }

  }, 700);
  callback();
};

// 进来后获取 是否展示引导页面
export const isShowGuideFun = async () => {
  const res = await window.EWebPlat.platService(window.webClientApiMap.clientWebcall.getGuideFlag);
  if (res.data == 0) {
    window.EWebPlat.store.dispatch("setGuideShow", true);
  } else {
    window.EWebPlat.store.dispatch("setGuideShow", false);
  }
};

