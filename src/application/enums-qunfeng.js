/*
 * @Description:
 * @Author: 李大玄
 * @Date: 2022-08-16 14:19:37
 * @FilePath: \web-client\src\application\enums.js
 * @LastEditors: chunxu.Zhao
 * @LastEditTime: 2023-11-21 11:17:03
 */
export default [
  {
    "id": 17378,
    "code": "chat",
    "productId": "10000",
    "name": "对话列表",
    "uri": "chat",
    "score": 1,
    "iconUrl": "icon-yiji-duihualiebiao",
    "type": 1
  },
  {
    "id": 17379,
    "code": "homeIndex/record",
    "productId": "10000",
    "name": "消息记录",
    "uri": "homeIndex/record",
    "score": 1,
    "iconUrl": "icon-yiji-xiaoxijilu",
    "type": 1
  },
  {
    "id": 17380,
    "code": "homeIndex/clues",
    "productId": "10000",
    "name": "名片管理",
    "uri": "homeIndex/clues",
    "score": 1,
    "iconUrl": "icon-yiji-mingpianguanli",
    "type": 1
  },
  {
    "id": 17381,
    "code": "config",
    "productId": "10000",
    "name": "设置中心",
    "uri": "config",
    "score": 1,
    "iconUrl": "icon-yiji-shezhizhongxin",
    "type": 1,
    "children": [
      {
        "id": "173811",
        "code": "config",
        "productId": "10000",
        "name": "部门管理",
        "uri": "homeIndex/department",
        "score": 1,
        "iconUrl": null,
        "type": 10,
        "children": []
      },
      {
        "id": "173812",
        "code": "config",
        "productId": "10000",
        "name": "客服分组管理",
        "uri": "homeIndex/customerGroup",
        "score": 2,
        "iconUrl": "",
        "type": 10,
        "children": []
      },
      {
        "id": "173814",
        "code": "config",
        "productId": "10000",
        "name": "账号管理",
        "uri": "homeIndex/customerAccount",
        "score": 2,
        "iconUrl": "",
        "type": 10,
        "children": []
      },
      {
        "id": "173815",
        "code": "config",
        "productId": "99009",
        "name": "常用语设置",
        "uri": "homeIndex/pageCommonWord",
        "score": 2,
        "iconUrl": "",
        "type": 10,
        "children": []
      }
    ]
  },
  {
    "id": 17382,
    "code": "homeIndex/channelQF",
    "productId": "10000",
    "name": "抖音企业号",
    "uri": "homeIndex/channelQF",
    "score": 1,
    "iconUrl": "icon-yiji-douyinqiyehao",
    "type": 1
  },
  {
    "id": 17383,
    "code": "aiConfig",
    "productId": "10000",
    "name": "AI机器人",
    "uri": "aiConfig",
    "score": 1,
    "iconUrl": "icon-yiji-aijiqiren",
    "type": 1,
    "children": [
      {
        "id": 173831,
        "code": "aiConfig",
        "productId": "10000",
        "name": "机器人训练",
        "uri": "homeIndex/pageMachineLearning",
        "score": 1,
        "iconUrl": null,
        "type": 10,
        "children": []
      },
      {
        "id": 173832,
        "code": "aiConfig",
        "productId": "10000",
        "name": "知识库管理",
        "uri": "homeIndex/pageKnowledgeBase",
        "score": 2,
        "iconUrl": "",
        "type": 10,
        "children": []
      }
    ]
  }
];
