import {getTodayAndYesterday, LocalStorageManager} from "@/utils/storageManager";
import {getStatus} from "./statusData";
import notificationMp3 from "@/static/notification.mp3";
import ELCONFIG from "@/utils/config/lang/index";
import ELEMENTUI from "element-ui";


function showNotification(type) {
  const msgObj = {
    "1": ELCONFIG.messageNotification.youConversation,
    "2": ELCONFIG.messageNotification.youMessage,
    "3": ELCONFIG.messageNotification.colleagueConversation,
    "4": ELCONFIG.messageNotification.colleagueMessage
  };
  ELEMENTUI.Notification({
    type: "",
    title: ELCONFIG.messageNotification.text,
    message: msgObj[type],
    duration: 2000,
    offset: 45
  });
}

const audioObj = {
  audio: null,
  isPlaying: false,
  audioSrc: {
    1: `${window.static_url || "/"}static/notification.mp3`,
    2: `${window.static_url || "/"}static/notification.mp3`,
    3: `${window.static_url || "/"}static/notification.mp3`,
    4: `${window.static_url || "/"}static/notification.mp3`
  },
  createAudio(type) {
    this.audio = new Audio();
    this.audio.src = this.audioSrc[type];
    this.audio.addEventListener("ended", this.stopAudio);
  },
  playAudio(type) {
    if (!this.audio) {
      // 如果音频对象不存在，则创建一个新的音频对象
      this.createAudio(type);
    }
    this.audio.play();
    this.isPlaying = true;
  },
  stopAudio() {
    if (this.audio && !this.audio.paused) {
      this.audio.pause();
      this.audio.currentTime = 0;
      this.isPlaying = false;
    }
  }
};

function removeLocalstorage() {
  LocalStorageManager.remove(LocalStorageManager.eventMsgOffset);
  LocalStorageManager.remove("current" + LocalStorageManager.chatPrefix);
  LocalStorageManager.remove("end" + LocalStorageManager.chatPrefix);
  LocalStorageManager.remove(LocalStorageManager.recordChatMap);
  LocalStorageManager.remove(LocalStorageManager.chatIdToUserId);
  LocalStorageManager.remove(LocalStorageManager.firstFetchEndTime);
  // LocalStorageManager.remove(LocalStorageManager.grpupUnreadMessageMap);
  // LocalStorageManager.remove(LocalStorageManager.globalHaveUnreadMessage);
  LocalStorageManager.removeDataStartingWith(getTodayAndYesterday().yesterday);
}

function showMessage(type = "success", msg = "") {
  if (msg) {
    ELEMENTUI.Message[type](msg);
  }
}

function mergeDeepObjects(obj1 = {}, obj2 = {}) {
  for (let key in obj2) {
    if (obj2.hasOwnProperty(key)) {
      if (typeof obj2[key] === "object" && obj2[key] !== null && !Array.isArray(obj2[key])) {
        if (!obj1.hasOwnProperty(key)) {
          obj1[key] = {};
        }
        mergeDeepObjects(obj1[key], obj2[key]);
      } else {
        obj1[key] = obj2[key];
      }
    }
  }
  return obj1;
}

/*
* userId, chatId, count = 0, unreadMsgObj = {}
* */
export function setUnreadMessageCount(options = {
  userId: "",
  chatId: "",
  count: 0,
  unreadMsgObj: undefined,
  accumulation: ""
}) {
  let unreadMessageCountObj = LocalStorageManager.get(LocalStorageManager.unreadMessageCount) || {};
  if (options.unreadMsgObj) {
    unreadMessageCountObj = mergeDeepObjects(unreadMessageCountObj, options.unreadMsgObj);
  } else if (options.accumulation) {
    unreadMessageCountObj[options.userId][options.chatId] += 1;
  } else {
    unreadMessageCountObj[options.userId] = Object.assign({}, unreadMessageCountObj[options.userId] || {}, {
      [options.chatId]: options.count
    });
  }
  LocalStorageManager.set(LocalStorageManager.unreadMessageCount, unreadMessageCountObj);
}

const pageSize = 100;
let allChatIds = [];
let chatMap = {};
let eventMsgTimer = null;

function getChatIds(list) {
  allChatIds = [].concat(allChatIds, list.map(item => item.chatId));
}

// 处理消息事件
function handleMessage({item}) {
  const loginUserId = window.EWebPlat.store.state.user.userMsg.userId;
  // 创建对话,
  let currentChatMap = LocalStorageManager.get(`current${LocalStorageManager.chatPrefix}`) || {}; // 进行中对话
  let endChatMap = LocalStorageManager.get(`end${LocalStorageManager.chatPrefix}`) || {}; // 已结束对话
  let recordChatMap = LocalStorageManager.get(LocalStorageManager.recordChatMap) || {};// 对话存储
  let chatIdToUserIdObj = LocalStorageManager.get(LocalStorageManager.chatIdToUserId) || {};
  let {type, chatId} = item.payload;
  if (["system_message_welcome", "message", "file", "system_message", "screenShots"].includes(type) /* 新消息 表情,消息都是message 欢迎语 */) {
    const chatIdToUserIdObj = LocalStorageManager.get(LocalStorageManager.chatIdToUserId) || {};
    // 重复对话
    const originChatList = [].concat(recordChatMap[chatId] || []).filter(ele => {
      return ele.msgId != item.payload.msgId; // !ele.msgId ||
    });
    recordChatMap[chatId] = [].concat(originChatList, {
      // chatId: item.payload.chatId,
      fromUserId: item.payload.fromUserId,
      msg: item.payload.msg,
      msgId: item.payload.msgId,
      timestamp: item.payload.timestamp,
      robot: item.payload.robot,
      type: item.payload.type,
      fileName: item.payload.fileName,
      screenShotPic: item.payload.screenShotPic
    });
    LocalStorageManager.set(LocalStorageManager.recordChatMap, recordChatMap);// 对话存储

    // ----------------------------------------------------------------------------------------------------
    const userIds = Object.keys(currentChatMap);
    const chatUserId = chatIdToUserIdObj[item.payload.chatId]?.userId || "";
    // 如果在监控过程中移除监控对象 没有userID 直接过滤
    if (!chatUserId || !currentChatMap[chatUserId]) {
      return;
    }
    const index = currentChatMap[chatUserId].findIndex(chatItem => chatItem.chatId == item.payload.chatId);
    if (index == undefined || index == -1) {
      return;
    }
    // 如果发送消息的是客服 将未读消息置为0   反之如果是访客 每次加1
    if (userIds.includes(item.payload.fromUserId)) {
      // currentChatMap[chatUserId][index].unreadMessageTotal = 0;
      setUnreadMessageCount({userId: chatUserId, chatId, count: 0});
    } else { /* 反之如果是访客 每次加1 但是如果当前选中的这个 每次都要置为 0*/
      if (window.getCurrentSelectChatItemInfo && item.payload.chatId == window.getCurrentSelectChatItemInfo().chatId) {
        // currentChatMap[chatUserId][index].unreadMessageTotal = 0;
        setUnreadMessageCount({userId: chatUserId, chatId, count: 0});
      } else {
        //  系统消息不累加
        if (!["system_message", "RECORD_SYSTEM", "system_message_welcome"].includes(item.payload.type)) {
          // currentChatMap[chatUserId][index].unreadMessageTotal += 1;
          // console.log("进来了", item);
          setUnreadMessageCount({userId: chatUserId, chatId, accumulation: 1});
        }
      }
    }
  } else if (type == "close" /* 当前对话结束 调完了*/) {
    const chatUserId = chatIdToUserIdObj[chatId].userId;
    const index = [].concat(currentChatMap[chatUserId] || []).findIndex(item => item.chatId == chatId);
    if (index != -1) {
      // 从当前对话删除 并加入到结束对话中
      const currentItem = currentChatMap[chatUserId].splice(index, 1)[0];
      const timeDiff = LocalStorageManager.get(LocalStorageManager.timeDiff) || 0;
      endChatMap[chatUserId] = [].concat(endChatMap[chatUserId] || [], Object.assign({}, currentItem, {
        packetTimestamp: item.packetTimestamp || new Date().getTime() + timeDiff,
        firstClickFlag: false
      }));
      LocalStorageManager.set(`current${LocalStorageManager.chatPrefix}`, Object.assign({}, currentChatMap));// 对话存储
      LocalStorageManager.set(`end${LocalStorageManager.chatPrefix}`, Object.assign({}, endChatMap));// 对话存储
      if (window.EmitChatInfo) {
        window.EmitChatInfo({item: currentItem, chatStsus: "end"}); //
      }
    }
  } else if (type == "autoAcceptInvite" /*创建对话 一定是我的 调完了*/) {
    const {headImgUrl, nickName} = JSON.parse(item.payload.extendInfos?.thirdProp || JSON.stringify({}));
    const chatData = {
      chatId: item.payload.chatId,
      firstClickFlag: false,
      headImgUrl,
      // nickName: nickName || `${item.payload.extendInfos?.visitor_location || ""}${new Date().getTime()}` || ELCONFIG.chat.chatList.noNicknameFound,
      nickName: nickName || `${item.payload.extendInfos?.visitor_location_province || ""} ${item.payload.extendInfos?.visitor_location_city || ""} ${item.packetTimestamp || new Date().getTime()}` || ELCONFIG.chat.chatList.noNicknameFound,
      visitorId: item.payload.viewId,
      // unreadMessageTotal: 0,
      businessName: item?.payload?.extendInfos?.name || "",
      userId: loginUserId,
      msg: "",
      searchEngineId: item.payload?.extendInfos?.searchEngineId || "",
      viewTime: item?.payload.timestamp
    };
    setUnreadMessageCount({userId: loginUserId, chatId: item.payload.chatId, count: 0});
    for (const itemKey in endChatMap) {
      for (let i = 0; i < endChatMap[itemKey].length; i++) {
        if (endChatMap[itemKey][i].visitorId == chatData.visitorId) {
          endChatMap[itemKey].splice(i, 1);
          i--;
        }
      }
    }
    for (const itemKey in currentChatMap) {
      for (let i = 0; i < currentChatMap[itemKey].length; i++) {
        if (currentChatMap[itemKey][i].visitorId == chatData.visitorId) {
          if (currentChatMap[itemKey]) {
            currentChatMap[itemKey].splice(i, 1);
            i--;
          }
        }
      }
    }
    currentChatMap[loginUserId] = [].concat(currentChatMap[loginUserId] || [], chatData);
    chatIdToUserIdObj[item.payload.chatId] = {userId: loginUserId};
  } else if (type == "autoMonitor" /* 监控者新增对话 */) {
    const {headImgUrl, nickName} = JSON.parse(item.payload.extendInfos?.thirdProp || JSON.stringify({}));
    console.log(item.payload.viewId, item?.payload?.extendInfos?.name || "", item.payload?.extendInfos?.searchEngineId || "");
    const chatData = {
      chatId: item.payload.chatId,
      firstClickFlag: false,
      headImgUrl,
      // nickName: nickName || `${item.payload.extendInfos?.visitor_location || ""}${new Date().getTime()}` || ELCONFIG.chat.chatList.noNicknameFound,
      nickName: nickName || `${item.payload.extendInfos?.visitor_location_province || ""} ${item.payload.extendInfos?.visitor_location_city || ""} ${item.packetTimestamp || new Date().getTime()}` || ELCONFIG.chat.chatList.noNicknameFound,
      visitorId: item.payload.viewId,
      // unreadMessageTotal: 0,
      businessName: item?.payload?.extendInfos?.name || "",
      userId: item.payload.userId,
      msg: "",
      searchEngineId: item.payload?.extendInfos?.searchEngineId || ""
    };
    setUnreadMessageCount({userId: item.payload.userId, chatId: item.payload.chatId, count: 0});
    for (const itemKey in endChatMap) {
      for (let i = 0; i < endChatMap[itemKey].length; i++) {
        if (endChatMap[itemKey][i].visitorId == chatData.visitorId) {
          endChatMap[itemKey].splice(i, 1);
          i--;
        }
      }
    }
    for (const itemKey in currentChatMap) {
      for (let i = 0; i < currentChatMap[itemKey].length; i++) {
        if (currentChatMap[itemKey][i].visitorId == chatData.visitorId) {
          currentChatMap[itemKey].splice(i, 1);
          i--;
        }
      }
    }
    currentChatMap[item.payload.userId] = [].concat(currentChatMap[item.payload.userId] || [], chatData);
    chatIdToUserIdObj[item.payload.chatId] = {userId: item.payload.userId};
    LocalStorageManager.set(LocalStorageManager.chatIdToUserId, chatIdToUserIdObj); // 映射
    LocalStorageManager.set(`current${LocalStorageManager.chatPrefix}`, currentChatMap);// 对话存储
    LocalStorageManager.set(`end${LocalStorageManager.chatPrefix}`, Object.assign({}, endChatMap));// 对话存储
  } else if (["takeover"].includes(type) /* 手动点击转接 */) {
    // 走到 takeover 会 紧跟一个 autoMonitor  所以 将数据删除后不需要处理 处理方法在 autoMonitor（PS·监听了 takeover 的对话需要做特殊处理 对应：if(item.payload.exts.stop === "0") { ）
    // const currentChatMap = LocalStorageManager.get(`current${LocalStorageManager.chatPrefix}`) || {}; // 进行中对话
    const index = currentChatMap[item.payload.sourceUserId].findIndex(item => item.chatId == chatId);
    let sourceData = currentChatMap[item.payload.sourceUserId].splice(index, 1)[0];

    if (item.payload.exts.stop === "0") {
      // 监听了转接的对话，需要特殊处理
      chatIdToUserIdObj[item.payload.chatId] = {userId: item.payload.takeoverUserId};
      sourceData.userId = item.payload.takeoverUserId;
      currentChatMap[item.payload.takeoverUserId] = [].concat(currentChatMap[item.payload.takeoverUserId] || [], sourceData);
    } else {
      delete chatIdToUserIdObj[chatId]; // 对话存储userId 干掉   有问题
    }
    LocalStorageManager.set(LocalStorageManager.chatIdToUserId, chatIdToUserIdObj); // 映射
    LocalStorageManager.set(`current${LocalStorageManager.chatPrefix}`, currentChatMap);// 对话存储
    if (window.HandleTakeover) {
      window.HandleTakeover(sourceData);
    }
  } else if (type == "acceptTakeover" /*机器人转人工 Transfer to manual */) {
    const index = currentChatMap[chatIdToUserIdObj[chatId].userId].findIndex(item => item.chatId == chatId);
    let sourceData = currentChatMap[chatIdToUserIdObj[chatId].userId].splice(index, 1)[0];
    sourceData.userId = item.payload.takeoverUserId;
    chatIdToUserIdObj[chatId] = {userId: loginUserId};
    currentChatMap[loginUserId] = [].concat(currentChatMap[loginUserId] || [], sourceData);
    window.EmitChatInfo({item: sourceData});
  } else {
    console.log("暂无处理", item);
    // "暂不支持查看，请登录易聊客户端查看"
  }
  LocalStorageManager.set(LocalStorageManager.chatIdToUserId, chatIdToUserIdObj); // 映射
  LocalStorageManager.set(`current${LocalStorageManager.chatPrefix}`, currentChatMap);// 对话存储
  LocalStorageManager.set(`end${LocalStorageManager.chatPrefix}`, Object.assign({}, endChatMap));// 对话存储
}

// 名片事件 /im/data/visitorInfo （收到事件，通知丽军，更新右侧名片数据）
function handleVisitorInfo({item}) {
  if (window.EmitVisitorinfo) {
    window.EmitVisitorinfo(Object.assign({}, item));
  }
}

// 强制退出 /im/forceLogout  接收到消息 将本地储存干掉 eventList 停掉
function handleForceLogout() {
  clearTimeout(eventMsgTimer);
  eventMsgTimer = null;
  removeLocalstorage();
  ELEMENTUI.MessageBox.confirm(ELCONFIG.chat.accountOffline.contentText, {
    confirmButtonText: ELCONFIG.chat.accountOffline.btnText, type: "warning", showCancelButton: false,
    closeOnClickModal: false, closeOnPressEscape: false, showClose: false
  }).then(async () => {
    const res = await EWebPlat.platService(webClientApiMap.clientPlatform.userLogin, {
      clientType: "WebClientServer",
      status: 1
    });
    if (res.code) {
      showMessage("error", res.msg || ELCONFIG.requestFailed);
      return;
    }
    getStatus(1);
    // showMessage("success", res.msg || "登录成功");
    removeLocalstorage();
    allChatIds = [];
    chatMap = {};
    // eslint-disable-next-line no-use-before-define
    await fetchList(); /// --------------------------------------------------------------------------------
  });
}

// /im/queue 排队事件（
function handleQueue({item}) {
  // getStatus(item?.payload?.runningStatus);
  console.log("排队事件-暂不处理", item);
}

// 客服状态变更 收到事件，通知 春旭，更新右侧顶部客服在线状态；丽军-排队选项隐藏）
function handleUserStatusChanged({item}) {
  const loginUserId = window.EWebPlat.store.state.user.userMsg.userId;
  if (item?.payload?.from == loginUserId) {
    if (item?.payload?.runningStatus) {
      getStatus(item?.payload?.runningStatus);
    }
  }
  console.log("客服状态变更-数据不对", item);
}

// 判断  消息提醒
function processArray(array, messageRemindConfig) {
  const loginUserId = window.EWebPlat.store.state.user.userMsg.userId;
  const chatIdToUserIdObj = LocalStorageManager.get(LocalStorageManager.chatIdToUserId) || {};
  let currentChatMap = LocalStorageManager.get(`current${LocalStorageManager.chatPrefix}`) || {}; // 进行中对话
  const usersId = Object.keys(currentChatMap);
  for (let i = 0; i < array.length; i++) {
    let item = array[i];
    const chatUserId = chatIdToUserIdObj[item.payload.chatId]?.userId || "";
    if (!chatUserId || !currentChatMap[chatUserId]) {
      return;
    }
    const index = currentChatMap[chatUserId].findIndex(chatItem => chatItem.chatId == item.payload.chatId);
    // 过滤消息
    if (index == undefined || index == -1) {
      return;
    }
    if (item.payload.type == "autoAcceptInvite") {
      if (messageRemindConfig[0].isEnable == "1") {
        audioObj.playAudio(1);
      }
      if (messageRemindConfig[0].isBubbleTip == "1") {
        showNotification(1);
      }
      return 1;
    }
    if (["system_message_welcome", "message", "file", "system_message", "screenShots"].includes(item.payload.type)) {
      // 如果新消息是机器人发的 不弹提醒，直接过滤掉
      if (usersId.includes(item.payload.fromUserId)) {
        continue;
      }
      // 如果当前选中了这通对话 新消息过来直接过滤
      if (window.getCurrentSelectChatItemInfo) {
        const selecctInfo = window.getCurrentSelectChatItemInfo();
        if (selecctInfo.chatId == item.payload.chatId) {
          continue;
        }
      }
      const {userId} = chatIdToUserIdObj[item.payload?.chatId] || {};
      // 我的
      if ((messageRemindConfig[1].isEnable == "1" || messageRemindConfig[1].isBubbleTip == "1") && userId == loginUserId) {
        if (messageRemindConfig[1].isEnable == "1") {
          audioObj.playAudio(2);
        }
        if (messageRemindConfig[1].isBubbleTip == "1") {
          showNotification(2);
        }
        return 2;
      }
    }
    if ((messageRemindConfig[2].isEnable == "1" || messageRemindConfig[2].isBubbleTip == "1") && item.payload.type == "autoMonitor") {
      if (messageRemindConfig[2].isEnable == "1") {
        audioObj.playAudio(3);
      }
      if (messageRemindConfig[2].isBubbleTip == "1") {
        showNotification(3);
      }
      return 3;
    }
    if (["system_message_welcome", "message", "file", "system_message", "screenShots"].includes(item.payload.type)) {
      // 如果新消息是机器人发的 不弹提醒，直接过滤掉
      if (usersId.includes(item.payload.fromUserId)) {
        continue;
      }
      // 如果当前选中了这通对话 新消息过来直接过滤
      if (window.getCurrentSelectChatItemInfo) {
        const selecctInfo = window.getCurrentSelectChatItemInfo();
        if (selecctInfo.chatId == item.payload.chatId) {
          continue;
        }
      }
      const {userId} = chatIdToUserIdObj[item.payload?.chatId] || {};
      if ((messageRemindConfig[3].isEnable == "1" || messageRemindConfig[3].isBubbleTip == "1") && userId != loginUserId) {
        if (messageRemindConfig[3].isEnable == "1") {
          audioObj.playAudio(4);
        }
        if (messageRemindConfig[3].isBubbleTip == "1") {
          showNotification(4);
        }
        return 4;
      }
    }
  }
}

export function getGroupUnread(type) {//  type: current end
  let chatMap = LocalStorageManager.get(`${type}${LocalStorageManager.chatPrefix}`) || {}; // 进行中对话
  let unreadMessageCountObj = LocalStorageManager.get(LocalStorageManager.unreadMessageCount) || {};
  let obj = {};
  for (const groupKey in chatMap) {
    let num = 0;
    chatMap[groupKey].forEach(item => {
      let count = 0;
      if (unreadMessageCountObj[item.userId] && unreadMessageCountObj[item.userId][item.chatId]) {
        count = unreadMessageCountObj[item.userId][item.chatId];
      }
      num += count;
    });
    obj[groupKey] = num;
  }
  return obj;
}

// 根据flag 判断红点展示与影藏
export function setMenuUnreadMsg(showAnim = true) {
  const flag = LocalStorageManager.get(LocalStorageManager.globalHaveUnreadMessage); // 判断全局中当前对话"current"是否有未读消息
  const parentDom = document.getElementsByClassName("itemDom0");
  if (flag && parentDom && parentDom[0]) {
    if (showAnim) {
      parentDom[0].style.setProperty("--before-animation", "moveUpDown");
    }
    parentDom[0].style.setProperty("--before-color", "#F86B4F");
    setTimeout(() => {
      parentDom[0].style.setProperty("--before-animation", "");
    }, 1000);
  } else {
    if (parentDom && parentDom[0]) {
      parentDom[0].style.setProperty("--before-color", "transparent");
      parentDom[0].style.setProperty("--before-animation", "");
    }
  }
}

async function fetchEventMsg() {
  /* 这里不应该是定时任务, 应该是延时递归, 因为要在接口响应成功且数据处理完成之后再次调用
  * 将数据当前对话迁移到 已结束对话 */
  eventMsgTimer = setTimeout(async () => {
    const firstFetchEndTime = LocalStorageManager.get(LocalStorageManager.firstFetchEndTime) || 0;
    const offset = LocalStorageManager.get(LocalStorageManager.eventMsgOffset) || null;
    const res = await EWebPlat.platService(webClientApiMap.clientPlatform.eventMsgList, {pageSize, offset});
    if (res.code) {
      showMessage("error", res.msg || ELCONFIG.requestFailed);
      return;
    }
    let list = [].concat(res.data || []);
    if (!list.length) {
      await fetchEventMsg();
      return;
    }
    LocalStorageManager.set(LocalStorageManager.eventMsgOffset, Object.assign({}, list[list.length - 1]).offset);
    let flag = true;
    for (let i = 0; i < list.length; i++) {
      const item = Object.assign({}, list[i], {
        chatId: list[i].payload.chatId
      });
      if (item.payload.type == "eventCustom" || item.packetTimestamp < firstFetchEndTime || !item.packetType /*没有类型直接跳出*/) {
        continue;
      }
      if (["/im/message"].includes(item.packetType) /* 消息&事件 */) {
        // 如果本地存储中有id  并且不是新建对话   如果本地有还是新建对话就不对了
        /* 循环所有的在线对话 如果两个chatId 一样, 就将正在对话的移到已结束对话中 */
        handleMessage({item});
      } else if (["/im/data/visitorInfo"].includes(item.packetType) /* 名片事件 */) {
        handleVisitorInfo({item});
      } else if (["/im/forceLogout"].includes(item.packetType) && item.packetTimestamp > firstFetchEndTime && offset /* 排队事件&客服状态变更 强制退出  */) {
        handleForceLogout();
        flag = false;
        break;
      } else if (["/im/queue"].includes(item.packetType)/* 排队 暂不处理 */) {
        handleQueue({item});
      } else if (["/im/userStatusChanged"].includes(item.packetType)/* 状态变更 春旭 */) {
        handleUserStatusChanged({item});
      }
    }
    // 消息提醒 --------------------------------------------------------------------------------
    if (window.$CONFIG.lang == "en" && offset) {
      // 您新到一个对话>您新到一条消息>同事新到一个对话>同事新到条消息
      let messageRemindConfig = LocalStorageManager.get(LocalStorageManager.messageRemindConfig);
      if (!messageRemindConfig) {
        const res = await EWebPlat.platService(webClientApiMap.clientWebcall.userSettinGet);
        if (res.code) {
          showMessage("error", res.msg || ELCONFIG.requestFailed);
          return;
        }
        messageRemindConfig = res.data.systemTip || [
          {"isBubbleTip": "0", "isEnable": "1", "type": "creatchat"}, // 创建对话
          {"isBubbleTip": "0", "isEnable": "1", "type": "receiveMessage"}, // 新消息
          {"isBubbleTip": "0", "isEnable": "1", "type": "creatFriendChat"}, // 创建监控者对话
          {"isBubbleTip": "0", "isEnable": "1", "type": "receiveFriendMessage"} // 监控者新消息
        ];
        LocalStorageManager.set(LocalStorageManager.messageRemindConfig, messageRemindConfig);
      }
      if (Object.values(messageRemindConfig).flat(1).length) {
        processArray(list.filter(item => item.packetType == "/im/message"), messageRemindConfig);
      }
    } else if (window.$CONFIG.lang == "cn" && offset) {
      const messageRemindConfig = [
        {"isBubbleTip": "1", "isEnable": "1", "type": "creatchat"}, // 创建对话
        {"isBubbleTip": "1", "isEnable": "1", "type": "receiveMessage"}, // 新消息
        {"isBubbleTip": "1", "isEnable": "1", "type": "creatFriendChat"}, // 创建监控者对话
        {"isBubbleTip": "1", "isEnable": "1", "type": "receiveFriendMessage"} // 监控者新消息
      ];
      LocalStorageManager.set(LocalStorageManager.messageRemindConfig, messageRemindConfig);
      processArray(list.filter(item => item.packetType == "/im/message"), messageRemindConfig);
    }
    // --------------------------------------------------------------------------------

    // 获取current 和 end 中 以userId 为分组的消息未读数
    const unreadMessage = {
      current: getGroupUnread("current"),
      end: getGroupUnread("end")
    };
    const currentGroup = unreadMessage.current;
    let num = 0;
    for (const key in currentGroup) {
      num += currentGroup[key];
    }
    LocalStorageManager.set(LocalStorageManager.grpupUnreadMessageMap, unreadMessage);
    const currentStore = LocalStorageManager.get(LocalStorageManager.globalHaveUnreadMessage) || 0;

    // 存要放在调用之前
    LocalStorageManager.set(LocalStorageManager.globalHaveUnreadMessage, num); // 判断全局中当前对话"current"是否有未读消息
    // 只有大于的时候才跳 如果消息进到当前选中的对话中 择不改变
    if (num > currentStore) {
      setMenuUnreadMsg();
    } else if (num == 0) {
      // 没有的时候也要将红点去掉
      setMenuUnreadMsg(false);
    }

    if (window.EmitChatList && window.getChatListForTab) {
      window.EmitChatList(); //
      window.getChatListForTab("", false); //
    }
    if (flag) {
      fetchEventMsg();
    }
  }, 2000);
}

/* 处理对话列表数据
    * 1. 将接口返回数据id取出 并返回 和并后的数据列表
    * 2. 将获取到的数据进行分组 我的 和 机器人的
    * 3. 将数据存储到store
    * */
function processingConversationListData(list) {
  let unreadMessageCountObj = LocalStorageManager.get(LocalStorageManager.unreadMessageCount) || {};
  let unreadMsgObj = {};
  list.forEach(itemChat => {
    let chatItem = {
      chatId: itemChat.chatId,
      firstClickFlag: false,
      fromUserId: itemChat.payload?.fromUserId,
      msg: itemChat.lastVisitorMessage,
      // unreadMessageTotal: 0
      viewTime: itemChat.viewTime || itemChat.createTime,
      cusTime: itemChat.cusTime
    };
    for (const userKey in itemChat.userList) {
      const item = itemChat.userList[userKey];
      if (item.userType === 0) { // 访客
        const {headImgUrl, nickName} = JSON.parse(item.otherInfos.thirdProp || JSON.stringify({}));
        Object.assign(chatItem, {
          headImgUrl,
          // nickName: nickName || `${item.otherInfos?.visitor_location || ""}${itemChat.createTime}` || ELCONFIG.chat.chatList.noNicknameFound,
          nickName: nickName || `${item.otherInfos?.visitor_location_province || ""} ${item.otherInfos?.visitor_location_city || ""} ${itemChat.createTime}` || ELCONFIG.chat.chatList.noNicknameFound,
          visitorId: item.userId,
          businessName: item?.otherInfos?.name || "",
          searchEngineId: item.otherInfos.searchEngineId
        });
      } else if (item.userType === 1 && item.isWatcher === false) { // 客服 与 监控组
        /* 每次获取到数据, 将数据暂时存储在 chatMap, 直至接口响应条数小于调用条数 进入callbackA 方法 将数据进行存储*/
        chatItem.userId = item.userId;
        chatMap[item.userId] = [].concat(chatMap[item.userId] || [], chatItem);
      }
    }
    unreadMsgObj[chatItem.userId] = Object.assign({}, unreadMsgObj[chatItem.userId] || {}, {
      [itemChat.chatId]: unreadMessageCountObj[chatItem.userId] ? unreadMessageCountObj[chatItem.userId][itemChat.chatId] || 0 : 0
    });
  });
  setUnreadMessageCount({unreadMsgObj});
}

async function fetchList() {
  const query = {
    chats: allChatIds.join(","),
    pageSize: pageSize,
    type: "userFriend",
    firstLogin: 1
  };
  const res = await EWebPlat.platService(webClientApiMap.clientPlatform.chatList, query);
  if (res.code) {
    showMessage("error", res.msg || ELCONFIG.requestFailed);
    return;
  }
  LocalStorageManager.set(LocalStorageManager.timeDiff, res.timestamp - new Date().getTime());
  const list = [].concat(res.data.data || []);
  /* 每次调用接口都将chatId 聚合 为了下一次请求 */
  getChatIds(list);
  /* 将数据进行分类 目前还没有格式化 */
  processingConversationListData(list);
  // 当数据长度小于100时，触发callbackB事件
  if (list.length < pageSize) {
    const loginUserId = window.EWebPlat.store.state.user.userMsg.userId;
    let resChatFriendList;
    try {
      resChatFriendList = await window.EWebPlat.platService(window.webClientApiMap.clientPlatform.chatFriendList);
    } catch (error) {
      console.error(error);
      return;
    }
    if (!resChatFriendList || resChatFriendList.code !== 0) {
      showMessage("error", resChatFriendList?.msg || ELCONFIG.requestFailed);
      return;
    }
    // 首次登录 将友好对象全拉过来 进行展示 --------------------------------
    [].concat(resChatFriendList.data || []).filter(item => {
      if (item.userId == loginUserId) {
        return false;
      }
      return true;
    }).forEach(item => {
      if (!chatMap[item.userId]) {
        chatMap[item.userId] = [];
      }
    });
    LocalStorageManager.set(`current${LocalStorageManager.chatPrefix}`, Object.assign({}, chatMap)); // 进行中对话
    let endChatMap = {};
    Object.keys(chatMap).forEach(item => endChatMap[item] = []);
    LocalStorageManager.set(`end${LocalStorageManager.chatPrefix}`, endChatMap); // 已结束对话
    // 首次登录 将友好对象全拉过来 进行展示 --------------------------------
    let chatIdtoUserIdObj = {};
    for (let chatMapKey in chatMap) {
      for (let i = 0; i < chatMap[chatMapKey].length; i++) {
        chatIdtoUserIdObj[chatMap[chatMapKey][i].chatId] = {userId: chatMapKey};
      }
    }
    LocalStorageManager.set(LocalStorageManager.chatIdToUserId, chatIdtoUserIdObj);
    LocalStorageManager.set(LocalStorageManager.firstFetchEndTime, res.timestamp || new Date().getTime());
    /* 获取当前tab的数据 包括自己的 与 机器人的 */
    if (window.getChatListForTab) {
      window.getChatListForTab("", true);
    }
    /* 列表数据走完 再去走事件接口 */
    fetchEventMsg();
  } else { // 当数据长度达到100时，触发callbackA事件
    fetchList();
  }
}

// 获取已结束对话列表
async function fetchEndChat() {
  const params = {
    searchWord: "",
    pageNum: 1,
    pageSize: 50
  };
  const res = await EWebPlat.platService(webClientApiMap.clientWebcall.chatHistoryRecent, params);
  if (res.code) {
    showMessage("error", res.msg || ELCONFIG.requestFailed);
    return;
  }
  const endChatList = [].concat(res.data || []).map(item => {
    return {
      chatId: item.chat_id,
      firstClickFlag: false,
      fromUserId: item.visitor_static_id,
      msg: item.last_visitor_message,
      // unreadMessageTotal: 0,
      headImgUrl: item.third_prop.head_img_url,
      nickName: item.third_prop.nick_name || ELCONFIG.chat.chatList.noNicknameFound,
      visitorId: item.visitor_static_id,
      businessName: "",
      searchEngineId: "",
      userId: item.user_id,
      packetTimestamp: new Date(item.end_time).getTime()
    };
  });
  const chatIdtoUserIdObj = LocalStorageManager.get(LocalStorageManager.chatIdToUserId) || {};
  console.log("chatIdtoUserIdObj", chatIdtoUserIdObj);
  let endChatMap = LocalStorageManager.get(`end${LocalStorageManager.chatPrefix}`) || {};
  endChatList.forEach(item => {
    endChatMap[item.userId] = [].concat(endChatMap[item.userId] || [], item);
    chatIdtoUserIdObj[item.chatId] = {userId: item.userId};
  });
  LocalStorageManager.set(`end${LocalStorageManager.chatPrefix}`, Object.assign({}, endChatMap));// 对话存储
  LocalStorageManager.set(LocalStorageManager.chatIdToUserId, chatIdtoUserIdObj);
}

export async function chatStep() {
  removeLocalstorage();
  await fetchList();
  await fetchEndChat();
  const resizeObserver = new ResizeObserver((entries) => {
    for (const entry of entries) {
      setTimeout(() => setMenuUnreadMsg(false), 100);
    }
  });
  setTimeout(() => {
    const dom = document.getElementsByClassName("layout-elaside")[0];
    if (dom) {
      resizeObserver.observe(dom);
    }
  }, 1000);
}

