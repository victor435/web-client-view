/*
 * @Description: application
 * @Author: 李大玄
 * @Date: 2022-07-21 14:12:53
 * @FilePath: \hw-web-client-view\src\application\index.js
 * @LastEditors: chunxu.Zhao
 * @LastEditTime: 2023-12-01 18:06:52
 */


__webpack_public_path__ = window.static_url || window.url || "";
import Vue from "vue";
import lang from "element-ui/lib/locale/lang/en";
import locale from "element-ui/lib/locale";

import getters from "@/store/getters";
import axios from "axios";
import EWebPlat from "plat@"; //
import {routes} from "@/router";
import apiMap from "@/api";
import {apiConfig, iconLinkArr} from "./baseUrl";
import utils from "plat@/src/utils";
import "./commonExport";
import navEnums, {fotmatterEnum} from "./enums";
import {responseSuccess, responseError, requestSuccess} from "./serviceExpand";
// import createPersistedState from "vuex-persistedstate";
import "../static/css/system-dropdown.scss";
import "../static/css/en-system.scss";
import "../static/css/el-theme.css";
import {chatStep} from "./handleChatData";
import {LocalStorageManager} from "@/utils/storageManager";
import {getUserStatus, isShowGuideFun} from "./statusData";
import ELCONFIG from "@/utils/config/lang/index";
import initConfig from "@/utils/config/initConfig";
import {processObject, menus} from "./enums";
import menusQunfeng from "./enums-qunfeng";
import ScrollUp from "@/utils/directive/scrollUp";
import RightKey from "@/utils/directive/rightKey";
Vue.use({
  install(Vue) {
    Vue.directive(ScrollUp.name, ScrollUp);
    Vue.directive(RightKey.name, RightKey);
  }
});
if (window.$CONFIG.lang == "en") {
// 设置语言
  locale.use(lang);
}

window.ELCONFIG = ELCONFIG;

window.EWebPlat = EWebPlat;
// 这个只是针对 本地启动() 不针对 从群峰进入页面
// 因为后端服务只有三个环境 但是在线服务有6个环境  这里只能判断出 test18, prd , 和线上的一个环境
let env = "";
if (window.__sso == "prod") {
  env = "group2-prd4";
} else if (window.__sso == "pre") {
  env = "prd19";
} else {
  env = "test-prd18";
}


// let env = window.location.hostname == "test-prd18.easyliao.com" ? "test-prd18" : window.location.hostname == "prd19.easyliao.com" ? "prd19" : "group2-prd4";
localStorage.setItem("env", env);
if (window.parent.frames[0] != window.self) {
  createApp();
  // getUserStatus(() => chatStep());
  // if (window.$CONFIG.lang == "cn") {
  //   isShowGuideFun(); // 海外新手引导不要
  // }
} else {
  window.addEventListener("message", (data) => {
    if (data.data.type == "init") {
      localStorage.setItem("juToken", data.data.data.token);
      initApp(data.data.data.token);
    }
  }, false);
}

// window.removeEventListener("unload", () => {
//   let juToken = localStorage.getItem("juToken");
//   initApp(juToken);
// } );

async function initApp(type) {
  LocalStorageManager.remove("baseLogin");
  // let baseUrl = window.location.hostname == "test-prd18.easyliao.net" ? "https://test-prd18.easyliao.net" : window.location.origin;
  let baseUrl = "";
  let aiBaseUrl = "";
  if (window.__sso == "pre") {
    baseUrl = "//prd19.easyliao.com";
    aiBaseUrl = "//prd19.easyliao.com";
  } else if (window.__sso == "prod") {
    baseUrl = "//external.easyliao.com";
    aiBaseUrl = "//train.easyliao.com";
  } else {
    baseUrl = "test-prd18.easyliao.net";
    aiBaseUrl = "test-prd18.easyliao.net";
  }

  let url = window.location.origin;

  let res = await axios.get(`${baseUrl}/external-web/oceanengine/login?access-token=${type}&origin=web-client&time=${new Date().getTime()}`);
  console.log(11111, res);
  if (res.data.code == 0) {
    let {accessToken, env, companyId} = res.data.data;
    LocalStorageManager.set("baseLogin", res.data.data);
    localStorage.setItem("env", env);
    axios.defaults.headers.common["Authorization"] = accessToken;
    window.location.href = `${url}/webcall/#/?access_token=${accessToken}`;
    createApp();
    window.location.href = `${url}/webcall/#/?access_token=${accessToken}`;
    window.EWebPlat.store.dispatch("setToken", accessToken);
    let _data = await axios.get(`${aiBaseUrl}/eachbot-flow-web/industry/validate/${companyId}`);
    if (_data.data.code == 0 && _data.data.data == false) {
      // window.location.href = `${url}/webcall/#/chat/industry?access_token=${accessToken}`;
      setTimeout(() => {
        window.EWebPlat.router.replace("/chat/industry");
      }, 500);
    } else {
      window.location.href = `${url}/webcall/#/chat?access_token=${accessToken}`;
      getUserStatus(() => chatStep());
      isShowGuideFun();
    }
  } else {
    createApp();
    setTimeout(() => {
      window.EWebPlat.router.replace("/chat/error").catch(err => {
        console.log(err);
      });
    }, 500);
  }
}

async function createApp() {
  const files = require.context("../store/modules", false, /\.js$/);
  EWebPlat.init({
    appConfig: initConfig.appConfig,
    iconLink: iconLinkArr || [],
    // directives:  // 指令 如果项目需要可以传入, 没有既不需要传
    // 菜单 如果需要菜单配置 就不会走接口请求
    // navEnums: navEnums,
    routes,
    storeConfig: {
      modules: utils.readFile(files), getters,
      // plugins: [
      //   createPersistedState({
      //     key: "xasxaxaxsa",
      //     storage: window.localStorage //选择 sessionStorage 进行存储
      //   })
      // ],
      storage: window.sessionStorage
    },
    storeKey: initConfig.storeKey,
    serviceConfig: {
      apiConfig,
      apiMap,
      // responseSuccess,
      responseError
      // requestSuccess
    },
    layoutSetting: {
      tag: false,
      breadcrumb: true
    },
    uiDropdown: initConfig.uiDropdown,
    menuConfig: { // 用来格式化获取到的菜单 后续功能在增加
      formatterMenu: (val) => {
        if (window.$CONFIG.lang == "cn") {
          return menusQunfeng;
        } else if (window.$CONFIG.lang == "en") {
          const data = fotmatterEnum(val);
          return data;
        }
      }
    },
    obtainUserName(val) {
      if (window.$CONFIG.lang == "cn") {
        return val.nickName + "(" + val.userId + ")";
      } else if (window.$CONFIG.lang == "en") {
        return val.realName; // val.userId;
      }
      return val.nickName + "(" + val.userId + ")";
    },
    obtainCompanyName(val) {
      if (window.$CONFIG.lang == "cn") {
        return "公司ID: " + val.companyId;
      } else if (window.$CONFIG.lang == "en") {
        return "companyID: " + val.companyId;
      }
    },
    // userInfo: {
    //   userName: "李大玄",
    //   userId: "110"
    // },
    apiMap: {
      navEnums: apiMap.authEupms.getPerUserMenus,
      userInfo: apiMap.clientPlatform.getUserInfo
      // updatePassword: apiMap.platCommon.editpass
    },
    init(vm) {
      window.plat = vm;
      const layoutSetting = vm.store.state.user.layoutSetting;
      layoutSetting.color.value = "#3585FB";
      vm.store.dispatch("setLayoutSetting", Object.assign({}, layoutSetting));
      // 获取 是否展示引导页面
      // EWebPlat.platService(webClientApiMap.clientWebcall.getGuideFlag).then(res => {
      //   if (res.data == 0) {
      //     EWebPlat.store.dispatch("setGuideShow", true);
      //   } else {
      //     EWebPlat.store.dispatch("setGuideShow", false);
      //   }
      // });
      // Vue.use("$ELCONFIG", ELCONFIG);
      Vue.prototype.$ELCONFIG = ELCONFIG;
    }
  });
}
window.version="2024年05月08日17:11:40";
// 修改密码 抽屉展示
// setTimeout(() => {
//   EWebPlat.updatePassword({
//     visible: true,
//     showClose: false
//   })
// }, 1000);
// setTimeout(() => {
//   EWebPlat.updatePassword({
//     visible: false,
//     showClose: true
//   })
