/*
 * @Description: baseUrl
 * @Author: 李大玄
 * @Date: 2022-07-21 14:14:59
 * @FilePath: \hw-web-client-view\src\application\baseUrl.js
 * @LastEditors: chunxu.Zhao
 * @LastEditTime: 2024-05-14 11:32:06
 */
//
export const iconLinkArr = [
  "//at.alicdn.com/t/c/font_2735677_p3t89spr9e.css", //   BEACON基础库
  // "//at.alicdn.com/t/c/font_4313697_fduqkq7toit.css", // 原本图标
  // "//at.alicdn.com/t/c/font_4313697_k9nnbx1zm8c.css", // 本项目图标
  "//at.alicdn.com/t/c/font_4313697_c1s08nu2yr6.js", // 本项目图标
  "//at.alicdn.com/t/c/font_4313697_j4to4gk16o.css" // 群峰重点项目
].concat(window.$CONFIG.lang == "en" ? "//at.alicdn.com/t/c/font_4369543_3awjnl0acnp.css" /*H1.0版本*/ : [] );


export const apiConfig = [
  // {
  //   name: "eom", // api下文件名
  //   http: "$structDemoClient", //
  //   proxyContext: "/prd18" // 本地环境公共前缀 做本地代理使用
  // }
];
const baseApiUrl = {
  // test: {
  //   authEupms: "",
  //   clientPlatform: "",
  //   clientWebcall: "",
  //   eachbotFlowWeb: "",
  //   apiTrust: "//test-prd18.easyliao.net/trust"
  // },
  defaultpre: {
    authEupms: "//pre-auth.easyliao.com",
    clientPlatform: "//prd19.easyliao.com",
    clientWebcall: "//prd19.easyliao.com",
    eachbotFlowWeb: "//prd19.easyliao.com", // 群峰才会去调 选择行业
    apiTrust: "//prd19.easyliao.com/webcall-trust-web" // 群峰才会去调 托管云端
  },
  cnpre: {
    authEupms: "//pre-auth.easyliao.com",
    clientPlatform: "//prd19.easyliao.com",
    clientWebcall: "//prd19.easyliao.com",
    eachbotFlowWeb: "//prd19.easyliao.com", // 群峰才会去调 选择行业
    apiTrust: "//prd19.easyliao.com/webcall-trust-web" // 群峰才会去调 托管云端
  },
  defaultprod: {
    authEupms: "//auth.easyliao.com",
    eachbotFlowWeb: "//train.easyliao.com",
    apiTrust: "/trust"
  },
  cnprod: {
    authEupms: "//auth.easyliao.com",
    eachbotFlowWeb: "//train.easyliao.com",
    apiTrust: "/trust"
  },
  entest: { // 海外 test
    authEupms: "//test19.easyliao.net",
    clientPlatform: "//test19.easyliao.net",
    clientWebcall: "//test19.easyliao.net",
    ocomsWeb: "//test19.easyliao.net",
    consoleWeb: "//test19.easyliao.net",
    reportWeb: "//test19.easyliao.net",
    payBillWeb: "//test19.easyliao.net",
    integrationWeb: "//test19.easyliao.net"
  },
  enpre: { // 海外 pre
    authEupms: "//pre-auth.upbeat.chat",
    clientPlatform: "//pre.upbeat.chat",
    clientWebcall: "//pre.upbeat.chat",
    ocomsWeb: "//pre-beatbox.upbeat.chat",
    consoleWeb: "//pre.upbeat.chat",
    reportWeb: "//pre.upbeat.chat",
    payBillWeb: "//pre-bill.upbeat.chat",
    integrationWeb: "//pre-external.upbeat.chat"
  },
  enprod: { // 海外 prod
    authEupms: "//auth.upbeat.chat",
    clientPlatform: "//im-api.upbeat.chat",
    clientWebcall: "//im-api.upbeat.chat",
    ocomsWeb: "//beatbox.upbeat.chat",
    consoleWeb: "//mgr.upbeat.chat",
    reportWeb: "//mgr.upbeat.chat",
    payBillWeb: "//bill.upbeat.chat",
    integrationWeb: "//external.upbeat.chat"
  }
};
const envV = (window.$CONFIG?.lang || "default") + window.__sso;
// 根据文件名定义接口上下文
export const contextEnum = {
  authEupms: `${window.isLocal ? "" : baseApiUrl[envV]?.authEupms || ""}/auth-eupms`,
  authSso: `${window.isLocal ? "" : baseApiUrl[envV]?.authEupms || ""}/auth-sso`,
  clientPlatform: `${window.isLocal ? "" : baseApiUrl[envV]?.clientPlatform || ""}/im`,
  clientWebcall: `${window.isLocal ? "" : baseApiUrl[envV]?.clientWebcall || ""}/webcall`,
  eachbotFlowWeb: `${window.isLocal ? "" : baseApiUrl[envV]?.eachbotFlowWeb || ""}/eachbot-flow-web`,
  apiTrust: `${window.isLocal ? "/webcall-trust-web" : baseApiUrl[envV]?.apiTrust || "/trust"}`,
  ocomsWeb: `${window.isLocal ? "" : baseApiUrl[envV]?.ocomsWeb || ""}/ocoms-web`,
  consoleWeb: `${window.isLocal ? "" : baseApiUrl[envV]?.consoleWeb || ""}/console/Service/`,
  reportWeb: `${window.isLocal ? "" : baseApiUrl[envV]?.reportWeb || ""}/report`,
  payBillWeb: `${window.isLocal ? "" : baseApiUrl[envV]?.payBillWeb || ""}/pay-bill-web`,
  integrationWeb: `${window.isLocal ? "" : baseApiUrl[envV]?.integrationWeb || ""}/external-hw-web`
};
const proxyObj = {
  en: {
    test: {
      base: "/entest",
      authEupmsBase: "/entest"
    },
    pre: {
      base: "/enprebase",
      authEupmsBase: "/enpreauth"
    },
    prod: {
      base: "",
      authEupmsBase: ""
    }
  },
  cn: {
    test: {
      base: "/cntest",
      authEupmsBase: "/cntest"
    },
    pre: {
      base: "/cnprebase",
      authEupmsBase: "/cnpreauth"
    },
    prod: {
      base: "",
      authEupmsBase: ""
    }
  }
};

const {authEupmsBase, base} = proxyObj[window.$CONFIG.lang][window.__sso];
// 根据文件名定义接口 本地 代理上下文
export const proxyContext = {
  authEupms: authEupmsBase, // /prd18
  authSso: authEupmsBase, // /prd18
  clientPlatform: base, // /prd18
  clientWebcall: base, // /prd18
  eachbotFlowWeb: base, // /prd18
  apiTrust: base, // /prd18
  ocomsWeb: base, // /prd18
  consoleWeb: base,
  reportWeb: base,
  payBillWeb: base, // /prd18
  integrationWeb: base
};

// 请求头
export const requestheader = {
  authEupms: {
    // productId: 91000
  }
};