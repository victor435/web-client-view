/*
 * @Author: 段丽军
 * @Date: 2023-11-04 14:46:11
 * @LastEditTime: 2023-11-16 11:51:20
 * @LastEditors: 段丽军
 * @Description:
 * @FilePath: /web-client/src/mixins/chatTimeFormat.js
 */
export default {
  methods: {
    chatTimeFormat(timeStr) {
      if(!timeStr) {
        return "";
      }

      let timeDate;
      if(isNaN(timeStr)) {
        timeDate = new Date(timeStr.replaceAll("-", "/"));
      } else {
        timeDate = new Date(window.parseInt(timeStr));
      }

      const timeYear = timeDate.getFullYear();
      const timeMonth = timeDate.getMonth()+1;
      const timeDay = timeDate.getDate();
      const timeHour = timeDate.getHours();
      const timeMinute = timeDate.getMinutes();
      const timeSecond = timeDate.getSeconds();

      const nowTimeData = new Date();
      const nowYear = nowTimeData.getFullYear();
      const nowMonth = nowTimeData.getMonth()+1;
      const nowDay = nowTimeData.getDate();

      function format2(item) {
        return item < 10 ? "0"+item : item;
      }

      let time = `${format2(timeHour)}:${format2(timeMinute)}:${format2(timeSecond)}`;
      if(window.$CONFIG.lang == "en") {
        time = timeDate.toLocaleTimeString("en-US", { hour12: true }).toLowerCase(); // 将时间转换为12小时制格式
      }

      if(timeYear === nowYear && timeMonth === nowMonth && timeDay === nowDay-1) {
        // 昨天
        time = window.ELCONFIG.chat.chatContent.msgTimeLabel+ " " + time;
      } else if(timeYear === nowYear && timeMonth === nowMonth && timeDay < nowDay-1) {
        // 同一年，大于一天
        time = `${format2(timeMonth)}/${format2(timeDay)} ` + time;
      } else if(timeYear !== nowYear) {
        time = `${format2(timeMonth)}/${format2(timeDay)}/${timeYear} ` + time;
      }
      return time;
    }
  }
};